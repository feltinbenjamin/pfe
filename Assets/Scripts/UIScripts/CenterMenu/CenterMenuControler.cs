﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;

/// <summary>
/// Initialise l'affichage des panneaux du canvas central du Game Menu.
/// </summary>
public class CenterMenuControler : MonoBehaviour
{
    /// <summary>
    /// Désacitve tous les panneaux du canvas, sauf le premier (panneau principal permettant de 
    /// sauvegarder ou de quitter), à chaque ouverture du menu.
    /// </summary>
    void OnEnable()
    {
        // display main menu
        foreach (Transform panel in transform)
        {
            panel.gameObject.SetActive(false);
        }
        transform.GetChild(0).gameObject.SetActive(true);
    }
}
