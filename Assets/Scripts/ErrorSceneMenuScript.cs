﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;


/// <summary>
/// Classe permettant de gérer les messages d'erreurs lors de la création ou de la tentative de rejoindre une room en ligne.
/// </summary>
public class ErrorSceneMenuScript : MonoBehaviour
{
    /// <value> Message d'erreur </value>
    public static TMP_Text messageBox; 

    /// <summary>
    /// Awake du GameObject
    /// </summary>
    private void Awake()
    {
        messageBox = GameObject.Find("MessageBox").GetComponent<TMP_Text>();
    }

    /// <summary>
    /// Fonction assignée à un bouton permettant de revenir au menu principal
    /// </summary>
    public void BackToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
