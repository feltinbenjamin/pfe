﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


/// <summary>
/// Permet de gérer les lieux de téléportation en réorientant le joueur vers le centre de l'objet sur lequel il travaille.
/// </summary>

public class OrientedTeleportationArea : BaseTeleportationInteractable
{
    /// <value>Rig du joueur dans la scène</value>
    [SerializeField] XRRig xrRig;

    /// <summary>
    /// Override de la fonction de BaseTeleportationInteractable qui génère les requêtes de téléportations.
    /// Recherche l'objet en réseau et oriente le rig du joueur vers le centre de cet objet.
    /// </summary>
    /// <param name="interactor"></param>
    /// <param name="raycastHit"></param>
    /// <param name="teleportRequest"></param>
    /// <returns></returns>
    protected override bool GenerateTeleportRequest(XRBaseInteractor interactor, RaycastHit raycastHit, ref TeleportRequest teleportRequest)
    {
        // génère la requête de téléportation
        teleportRequest.destinationPosition = raycastHit.point;
        teleportRequest.destinationRotation = transform.rotation;

        // turn to face the object
        GameObject networkObject = GameObject.FindWithTag("NetworkObject");
        if (networkObject != null)
        {
            Vector3 objectPosition = networkObject.transform.position;
            float theta = 0.0f;
            // destination's position relative to object's position
            float x = raycastHit.point.x - objectPosition.x;
            float z = raycastHit.point.z - objectPosition.z;

            if (Mathf.Abs(z) < 1e-5)    // avoid division by zero
            {
                theta = (x > 0) ? -90.0f : 90.0f;
            }
            else
            {
                // calcul de l'angle entre la position de joueur et la position de l'objet
                theta = Mathf.Atan(x / z) * (180.0f / Mathf.PI);    // angle in degrees

                // si le joueur se trouve de l'autre côté de l'objet réseau on le retourne
                if (z > 0)
                {
                    theta = theta - 180.0f;
                }
            }
            // change rig's relative orientation
            float rigRotation = xrRig.transform.rotation.eulerAngles.y;
            xrRig.RotateAroundCameraUsingRigUp(theta - rigRotation);
        }
        return true;
    }
}
