﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using Photon.Pun;

/// <summary>
/// Panneau principal du Game Menu permettant d'exporter l'objet ou de quitter la salle.
/// </summary>
public class GameMenuUI : MonoBehaviourPunCallbacks
{
    /// <value>Panneau de sauvegarde de l'objet</value>
    [SerializeField] GameObject saveMenu;
    /// <value>Pop up de sauvegarde avant de quitter</value>
    [SerializeField] SavePopUpUI savePopUp;
    /// <value> UI d'aide </value>
    [SerializeField] GameObject helpUI;

    /// <summary>
    /// Ouvre le panneau de sauvegarde de l'objet réseau.
    /// </summary>
    public void ExportObjBtn()
    {
        saveMenu.SetActive(true);
        gameObject.SetActive(false);
        helpUI.SetActive(false);
    }

    /// <summary>
    /// Ouvre le panneau d'aide
    /// </summary>
    public void HelpBtn()
    {
        helpUI.SetActive(true);
    }

    /// <summary>
    /// Si l'objet a été modifié ouvre le pop up de sauvegarde, sinon quitte la salle réseau.
    /// </summary>
    public void LeaveBtn()
    {
        // ask to save
        GameObject shape = GameObject.FindWithTag("NetworkObject");
        if (shape != null && NetworkObject.modified)
        {
            savePopUp.OpenPopUp();
            savePopUp.closeApp = false;
        }
        else
        {
            LeaveRoom();
        }
    }

    /// <summary>
    /// Si l'objet a été modifié ouvre le pop up de sauvegarde, sinon ferme l'application.
    /// </summary>
    public void QuitBtn()
    {
        // ask to save
        GameObject shape = GameObject.FindWithTag("NetworkObject");
        if (shape != null && NetworkObject.modified)
        {
            savePopUp.OpenPopUp();
            savePopUp.closeApp = true;
        }
        else
        {
            CloseApp();
        }
    }

    /// <summary>
    /// Quitte la salle réseau, l'utilisateur sera automatiquement redirigé vers le menu principal.
    /// </summary>
    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    /// <summary>
    /// Ferme l'application.
    /// </summary>
    public void CloseApp()
    {
        Application.Quit(); ;
    }
}
