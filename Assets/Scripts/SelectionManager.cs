﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Classe permettant de gérer la sélection des vertices.
/// </summary>
public class SelectionManager : MonoBehaviour
{
    /// <value>Liste contentant les vertices sélectionnés (les gameobject spheres dans unity)</value> 
    private static List<GameObject> selectedVertices = new List<GameObject>();

    /// <value>Objet réseau</value>
    private static NetworkObject shape = null;

    /// <summary>
    /// Affecte l'objet réseau sur lequel on travaille.
    /// </summary>
    /// <param name="networkObject">Objet réseau</param>
    public static void setNetworkObject(NetworkObject networkObject)
    {
        shape = networkObject;
    }

    /// <summary>
    /// Accesseur de la liste de sélection.
    /// </summary>
    /// <returns>
    /// La liste des vertices selectionnés
    /// </returns>
    public static List<GameObject> GetSelectedVertices()
    {
        return selectedVertices;
    }


    /// <summary>
    /// Vide la liste des vertices selectionnés et les débloque pour les autres joueurs.
    /// </summary>
    public static void ClearSelection()
    {
        // On enlève le nombre d'éléments dans notre selection au compteur de blocage de l'objet
        shape.photonView.RPC("syncCanMove", Photon.Pun.RpcTarget.All, -selectedVertices.Count);
        
        foreach (GameObject vertex in selectedVertices)
        {
            // Si l'objet existe encore, on le débloque
            if (vertex != null && vertex.TryGetComponent(out NetworkVertexController controller))
            {
                controller.FreeVertex();
            }
        }

        // désélection de tous les vertices
        selectedVertices.Clear();
    }

    /// <summary>
    /// Ajoute un vertex à la liste de sélection et le bloque (ainsi que pour ses vertex voisins) pour les autres joueurs.
    /// </summary>
    /// <param name="vertex">
    /// Le gameobject vertex à ajouter à la liste de sélection
    /// </param>
    public static void selectVertex(GameObject vertex)
    {
        selectedVertices.Add(vertex);

        // On incrémente le compteur de blocage de l'objet
        shape.photonView.RPC("syncCanMove", Photon.Pun.RpcTarget.All, 1);

    }

    /// <summary>
    /// Enlève un vertex de la liste de sélection et le débloque (ainsi que pour ses vertex voisins) pour les autres joueurs.
    /// </summary>
    /// <param name="vertex">
    /// Le gameobject vertex à enlever de la liste de sélection
    /// </param>
    public static void deselectVertex(GameObject vertex)
    {
        selectedVertices.Remove(vertex);

        // On décrémente le compteur de blocage de l'objet
        shape.photonView.RPC("syncCanMove", Photon.Pun.RpcTarget.All, -1);
    }


    /// <summary>
    /// Enlève un groupe de vertices de la liste de sélection et les débloque (ainsi que pour leurs vertex voisins) pour les autres joueurs.
    /// </summary>
    /// <param name="vertices">
    /// Tableau de gameobject vertex a enlever de la liste de selection
    /// </param>
    public static void deselectVertices(GameObject[] vertices)
    {
        // On décrémente le compteur de blocage de l'objet
        shape.photonView.RPC("syncCanMove", Photon.Pun.RpcTarget.All, -vertices.Length);

        foreach (GameObject vertex in vertices) {
            selectedVertices.Remove(vertex);
        }
    }

    /// <summary>
    /// Vérifie que la liste de sélection est vide
    /// </summary>
    /// <returns>
    /// Booléen valant vrai si la liste est vide
    /// </returns>
    public static bool isEmpty()
    {
        return selectedVertices.Count == 0;
    }
}
