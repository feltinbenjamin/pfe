﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////

using System.IO;
using UnityEngine;

/// <summary>
/// Gère fonctions des boutons du pop up de fichier déjà existant.
/// </summary>
public class ReplaceFilePopUpUI : MonoBehaviour
{
    /// <value>Panneau de sauvegarde du Game Menu</value>
    [SerializeField] SaveMenuUI saveMenu;

    /// <value>Chemin d'accès aux fichiers de l'application.</value>
    string myPath;
    /// <value>Nom du fichier actuel.</value>
    string myName;

    /// <summary>
    /// Ecrase le fichier déjà existant et ferme le pop up.
    /// </summary>
    public void replaceBtn()
    {
        saveMenu.SaveFile(myPath + myName + ".obj");
        ClosePopUp();
    }

    /// <summary>
    /// Ajoute un numéro de version à la fin du nom du fichier, enregistre la copie et ferme le pop up.
    /// </summary>
    public void CopyBtn()
    {
        // si la copie numéro 1 existe déjà, incrémente le numéro de version
        int i = 1;
        while (File.Exists(myPath + myName + " (" + i + ").obj")) i++;

        // nouveau nom avec numéro de version
        myName += " (" + i + ")";
        saveMenu.SaveFile(myPath + myName + ".obj");
        ClosePopUp();
    }

    /// <summary>
    /// Ferme le pop up. L'enregistrement est annulé.
    /// </summary>
    public void CancelBtn()
    {
        ClosePopUp();
    }

    /// <summary>
    /// Désactive le panneau et son canvas.
    /// </summary>
    void ClosePopUp()
    {
        transform.parent.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Active le panneau et son canvas. Initialise les valeurs du chemin d'accès et du nom de fichier.
    /// </summary>
    /// <param name="path">Chemin d'accès aux fichiers de l'application</param>
    /// <param name="name">Nom du fichier à enregistrer</param>
    public void OpenPopUp(string path, string name)
    {
        myPath = path;
        myName = name;
        transform.parent.gameObject.SetActive(true);
        gameObject.SetActive(true);
    }
}
