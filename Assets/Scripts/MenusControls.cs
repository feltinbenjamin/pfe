﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using System;
using UnityEngine;


/// <summary>
/// Classe permettant de gérer la position des menus en world position.
/// Les menus suivent la caméra dans le plan horizontal.
/// </summary>
public class MenusControls : MonoBehaviour
{
    [SerializeField] Transform rigCamera;

    /// <summary>
    /// Calcul de la position et de l'orientation du Game Menu à chaque ouverture.
    /// </summary>
    void OnEnable()
    {
        setPosition();
        setRotation();
    }

    /// <summary>
    /// Mise à jour de la position et de l'orientation du Game Menu 
    /// si le joueur a bougé de plus de 10 cm ou s'est tourné de plus de 45°.
    /// </summary>
    void Update()
    {
        // Mise à jour de la position si le joueur s'est déplacé de plus de 10 cm en x ou en z
        if (Math.Abs(transform.position.x - rigCamera.position.x) > 0.1 
        || Math.Abs(transform.position.z - rigCamera.position.z) > 0.1)
        {
            setPosition();
        }

        // Mise à jour de l'orientation si le joueur a tourné la tête de plus de 45°
        if (getAngle(transform.rotation.eulerAngles.y, rigCamera.rotation.eulerAngles.y) > 45)
        {
            setRotation();
        }
    }

    /// <summary>
    /// Mise à jour de la position du menu en fonction de la position de la caméra dans le plan horizontal.
    /// La position sur l'axe vertical (axe y) reste fixe.
    /// </summary>
    void setPosition()
    {
        // la position sur l'axe vertical ne change pas
        this.transform.position = new Vector3(
            rigCamera.position.x,
            this.transform.position.y,
            rigCamera.position.z
        );
    }

    /// <summary>
    /// Mise à jour de l'orientation du menu en fonction de l'orientation de la caméra sur l'axe vertical.
    /// Afin de garder une certaine stabilité dans la position du menu, les rotations autour des axes x et z ne sont pas prises en compte.
    /// </summary>
    void setRotation()
    {
        // rotation des menus autour de l'axe vertical d'un angle égal à la différence des deux orientations
        this.transform.Rotate(Vector3.up, rigCamera.rotation.eulerAngles.y - transform.rotation.eulerAngles.y);
    }

    /// <summary>
    /// Calcul de l'angle minimal entre deux orientations.
    /// </summary>
    /// <param name="angle1">Première orientation en degrés</param>
    /// <param name="angle2">Deuxième orientation en degrés</param>
    /// <returns>La valeur absolue de l'angle saillant de la rotation</returns>
    double getAngle(double angle1, double angle2)
    {
        // valeur positive de l'angle entre les deux orientations
        double angle = Math.Abs((angle2 % 360) - (angle1 % 360));

        // bornage de la valeur entre 0° et 180° (angle saillant)
        if (angle > 180) angle = 360 - angle;

        return angle;
    }
}
