﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.IO;
using System;
using System.Globalization;

/// <summary>
/// Classe gerant l'objet réseau sur lequel on travail
/// </summary>
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class NetworkObject : MonoBehaviourPunCallbacks
{

    // ---------------------------------------- Attributs ----------------------------------------


    /// <value> Material pour le mode de rendu par défaut de la mesh </value>
    [SerializeField] Material matMesh;
    /// <value> Material pour le mode de rendu wireframe de la mesh </value>
    [SerializeField] Material matMeshWire;

    /// <value> Material renderer pour le mode de rendu par défaut de la mesh </value>
    Material[] defaultMat;
    /// <value> Material renderer pour le mode de rendu wireframe de la mesh </value>
    Material[] wireMat;
    /// <value> Material renderer pour le mode de rendu combiné de la mesh </value>
    Material[] combinedMat;


    /// <value> Mesh renderer </value>
    static Mesh mesh;
    /// <value> vertices de la mesh </value>
    public static Vector3[] vertices;
    /// <value> triangles de la mesh </value>
    public static int[] triangles;
    /// <value> Couleur des vertices </value>
    public static Color32[] colors32;

    /// <value> booléen pour savoir si on durcit les arêtes </value>
    public static bool flatFaces = true;
    /// <value> flat vertices de la mesh </value>
    static Vector3[] flatVertices;
    /// <value> flat triangles de la mesh </value>
    static int[] flatTriangles;
    /// <value> couleur des flat vertices de la mesh </value>
    static Color32[] flatColors32;

    /// <value> Taile de la surface par défaut selon X </value>
    [SerializeField] const int xSize = 40;
    /// <value> Taile de la surface par défaut selon Z </value>
    [SerializeField] const int zSize = 10;
    /// <value> Taile de la grille </value>
    [SerializeField] const float gridSize = 0.25f;


    /// <value> valeur min sur X </value>
    public float minX;
    /// <value> valeur min sur Y </value>
    public float minY;
    /// <value> valeur min sur Z </value>
    public float minZ;

    /// <value> valeur max sur X </value>
    public float maxX;
    /// <value> valeur max sur Y </value>
    public float maxY;
    /// <value> valeur max sur Z </value>
    public float maxZ;


    /// <value> Prefab des spheres representant un vertex </value>
    public GameObject vertexPrefab;


    /// <value> Liste des spheres representant l'ensemble des vertices </value>
    public static List<GameObject> VerticesObjects;
    
    /// <value> Chemein d'enregistrement des fichiers </value>
    private string path;

    /// <value> booleen valant vrai si l'objet est different de celui charge (ie. il a ete modifié) </value>
    public static bool modified;

    /// <value> Compteur valant le nombre de vertex bloqués. Si vaut 0 alors l'objet peut être déplacé </value>
    public static int canMove;





    // ---------------------------------------- Méthodes ----------------------------------------

    /// <summary>
    /// Awake du GameObject associé. récupère le mesh du renderer, créer les triangle et vertices si on est le masterClient.
    /// </summary>
    void Awake()
    {
        path = MainMenuUI.path + MainMenuUI.fileName;

        // Attribution de la mesh au renderer
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        // Si on est le masterClient (ie. on a créé la salle), alors on créé l'objet
        if (PhotonNetwork.IsMasterClient)
        {
            CreateShape();
            UpdateMesh();
            CreateVerticesSphere();
        }

        // On attribue l'objet au menu d'options pour pouvoir le modifier
        ShapeOptionUI.setNetworkObject(this.gameObject);

        // On attribue l'objet au PlayerController pour pouvoir le déplacer
        PlayerControler.setNetworkObject(this.gameObject);

        // On attribue l'objet au SelectionManager pour pouvoir gérer la sélection de ses vertices
        SelectionManager.setNetworkObject(this);

        // Attribution des material renderer pour les modes de rendu
        combinedMat = new Material[2] { matMesh, matMeshWire };
        defaultMat = new Material[1] { matMesh };
        wireMat = new Material[1] { matMeshWire };

        // L'objet peut être déplacé
        canMove = 0;
    }

    /// <summary>
    /// Callabck lorsque le gameobject est detruit
    /// </summary>
    private void OnDestroy()
    {
        ShapeOptionUI.setNetworkObject(null);
        PlayerControler.setNetworkObject(null);
        SelectionManager.setNetworkObject(null);
    }

    /// <summary>
    /// Callback quans un autre joueur se connecte a notre salle
    /// </summary>
    /// <param name="newPlayer">
    /// Le nouveau joueur
    /// </param>
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(newPlayer + " connect");
        base.OnPlayerEnteredRoom(newPlayer);

        // On invoque une RPC pour lui donner les infos de l'objet
        photonView.RPC("syncObjectInfos", newPlayer, vertices, triangles, canMove);
    }

    /// <summary>
    /// RPC synchronisant les blocages des vertex
    /// </summary>
    /// <param name="cpt">
    /// Le nombre de vertices à ajouter au compteur (peut être négatif pour en libérer)
    /// </param>
    [PunRPC]
    void syncCanMove(int cpt)
    {
        canMove += cpt;
    }

    /// <summary>
    /// RPC synchronisant les informatiosn de l'objet lors d'une connexion
    /// </summary>
    /// <param name="_vertices">
    /// Les vertices de l'objet
    /// </param>
    /// <param name="_triangles">
    /// Les triangles de l'objet
    /// </param>
    /// <param name="cpt">
    /// Le nombre de vertices bloqués
    /// </param>
    [PunRPC]
    void syncObjectInfos(Vector3[] _vertices, int[] _triangles, int cpt)
    {
        Debug.Log("Synchronizing Object Infos");
        vertices = _vertices;
        triangles = _triangles;
        colors32 = new Color32[vertices.Length];
        for (int c = 0; c < colors32.Length; c++)
        {
            colors32[c] = new Color32(255, 255, 255, 255);
        }
        canMove = cpt;
        UpdateMesh();
        CreateVerticesSphere();
    }

    /// <summary>
    /// RPC synchronisant un vertex précis
    /// </summary>
    /// <param name="vertex">
    /// Le vertex
    /// </param>
    /// <param name="index">
    /// L'indice de ce vertex dans le tableau verices de la mesh
    /// </param>
    [PunRPC]
    void syncObjectVertex(Vector3 vertex, int index)
    {
        Debug.Log("Synchronizing Vertex Object");
        vertices[index] = vertex;
        UpdateMinMax();
        UpdateMesh();
        VerticesObjects[index].transform.localPosition = vertex;
    }

    /// <summary>
    /// Update de la position d'un vertex et envoie de ce changemetn sur le réseau en RPC
    /// </summary>
    /// <param name="index">
    /// Indice du vertex dans le tableau vertices de la mesh
    /// </param>
    /// <param name="pos">
    /// Nouvelle position du vertex
    /// </param>
    public void UpdateVertexPosition(int index, Vector3 pos)
    {
        vertices[index] = pos;
        UpdateMinMax();
        UpdateMesh();
        photonView.RPC("syncObjectVertex", RpcTarget.Others, vertices[index], index);
    }

    /// <summary>
    /// Préparation du blocage de ce verttex et de ces voisins popur les autres joueurs
    /// </summary>
    /// <param name="index">
    /// Indice du vertex à bloquer
    /// </param>
    public void BlockVertex(int index)
    {
        photonView.RPC("SyncBlockVertex", RpcTarget.Others, index, false);

        // block neighbours
        foreach (NetworkVertexController neighbour in VerticesObjects[index].GetComponent<NetworkVertexController>().neighbours)
        {
            photonView.RPC("SyncBlockVertex", RpcTarget.Others, neighbour.index, false);
        }
    }

    /// <summary>
    /// Préparation du déblocage de ce verttex et de ces voisins popur les autres joueurs
    /// </summary>
    /// <param name="index">
    /// Indice du vertex à débloquer
    /// </param>
    public void UnblockVertex(int index)
    {
        photonView.RPC("SyncBlockVertex", RpcTarget.Others, index, true);

        // unblock neighbours
        foreach (NetworkVertexController neighbour in VerticesObjects[index].GetComponent<NetworkVertexController>().neighbours)
        {
            photonView.RPC("SyncBlockVertex", RpcTarget.Others, neighbour.index, true);
        }
    }


    /// <summary>
    /// RPC pour synchroiniser le blocage ou déblocage d'un vertex
    /// </summary>
    /// <param name="index">
    /// Indice du vertex correspondant
    /// </param>
    /// <param name="enabled">
    /// booleen representant l'etat du vertex (true=debloqué, false=bloqué)
    /// </param>
    [PunRPC]
    public void SyncBlockVertex(int index, bool enabled)
    {
        NetworkVertexController vertex = VerticesObjects[index].GetComponent<NetworkVertexController>();
        
        if (!enabled)
        {
            // On ne peut plus interagir avec l'objet et change la couleur ( -> rouge)
            vertex.interactionLayerMask = LayerMask.GetMask("Nothing");
            vertex.GetComponent<Renderer>().material.SetColor("BaseColor", vertex.blockedVertex);
        }
        else
        {
            vertex.interactionLayerMask = ~0;   // everything
            // On  change la couleur ( -> vert)
            vertex.GetComponent<Renderer>().material.SetColor("BaseColor", vertex.unblockedVertex);
        }
    }

    /// <summary>
    /// Fonction permettant de créer la surface de l'objet (uniquement pour le pasterClient)
    /// </summary>
    void CreateShape()
    {
        // Si le fichier.obj existe, on le charge
        if (File.Exists(path))
        {
            loadObj(path);
        }
        // Sinon on créé une surface par défaut
        else
        {
            #region Vertices attribution
            vertices = new Vector3[(xSize + 1) * (zSize + 1)];

            int i = 0;
            for (int z = 0; z <= zSize; z++)
            {
                for (int x = 0; x <= xSize; x++)
                {
                    float y = Mathf.PerlinNoise(x * .5f, z * .5f) * 2.0f;
                    // center of the object in the middle
                    vertices[i] = new Vector3(x - (xSize / 2), y, z - (zSize / 2)) * gridSize;
                    i++;
                }
            }

            #endregion

            #region Triangles attribution

            triangles = new int[6 * xSize * zSize];

            int vert = 0;
            int tris = 0;

            for (int z = 0; z < zSize; z++)
            {
                for (int x = 0; x < xSize; x++)
                {
                    triangles[tris + 0] = vert + 0;
                    triangles[tris + 1] = vert + xSize + 1;
                    triangles[tris + 2] = vert + 1;
                    triangles[tris + 3] = vert + 1;
                    triangles[tris + 4] = vert + xSize + 1;
                    triangles[tris + 5] = vert + xSize + 2;
                    tris += 6;

                    vert++;

                    //yield return new WaitForSeconds(1e-8f);
                }
                vert++;
            }
            #endregion

            #region Color attribution
            colors32 = new Color32[(xSize + 1) * (zSize + 1)];
            for (int c = 0; c < colors32.Length; c++)
            {
                colors32[c] = new Color32(255, 255, 255, 255);
            }
            #endregion
        }

        // Calcule des normals
        mesh.RecalculateNormals();
    }

    /// <summary>
    /// Fonction permettant de créer les sphères représentant les vertices de l'objet
    /// </summary>
    void CreateVerticesSphere()
    {
        VerticesObjects = new List<GameObject>();

        // Pour chaque vertex de l'objet, on instantie une sphere, et on la lie au vertex de la mesh en question
        for (int i = 0; i < vertices.Length; i++)
        {
            GameObject sphere = Instantiate(vertexPrefab, vertices[i], transform.rotation);
            VerticesObjects.Add(sphere);
            sphere.transform.SetParent(this.transform);
            sphere.GetComponent<NetworkVertexController>().mainObject = this;
            sphere.GetComponent<NetworkVertexController>().index = i;
        }

        // initialise min max
        UpdateMinMax();

        // Recherche des voisins
        for (int i = 0; i < triangles.Length; i=i+3)
        {
            NetworkVertexController vertex1 = VerticesObjects[triangles[i]].GetComponent<NetworkVertexController>();
            NetworkVertexController vertex2 = VerticesObjects[triangles[i+1]].GetComponent<NetworkVertexController>();
            NetworkVertexController vertex3 = VerticesObjects[triangles[i+2]].GetComponent<NetworkVertexController>();

            if (!vertex1.neighbours.Contains(vertex2))
                vertex1.neighbours.Add(vertex2);
            if (!vertex1.neighbours.Contains(vertex3))
                vertex1.neighbours.Add(vertex3);

            if (!vertex2.neighbours.Contains(vertex1))
                vertex2.neighbours.Add(vertex1);
            if (!vertex2.neighbours.Contains(vertex3))
                vertex2.neighbours.Add(vertex3);

            if (!vertex3.neighbours.Contains(vertex1))
                vertex3.neighbours.Add(vertex1);
            if (!vertex3.neighbours.Contains(vertex2))
                vertex3.neighbours.Add(vertex2);
        }

        // at creation the object hasn't been modified
        modified = false;
    }



    /// <summary>
    /// Fonction permettant d'update les voisins des vertices de l'objet
    /// </summary>
    public static void UpdateNeighbors()
    {

        for (int i = 0; i < vertices.Length; i++)
        {
            VerticesObjects[i].GetComponent<NetworkVertexController>().neighbours.Clear();
        }

        for (int i = 0; i < triangles.Length; i = i + 3)
        {
            NetworkVertexController vertex1 = VerticesObjects[triangles[i]].GetComponent<NetworkVertexController>();
            NetworkVertexController vertex2 = VerticesObjects[triangles[i + 1]].GetComponent<NetworkVertexController>();
            NetworkVertexController vertex3 = VerticesObjects[triangles[i + 2]].GetComponent<NetworkVertexController>();

            if (!vertex1.neighbours.Contains(vertex2))
                vertex1.neighbours.Add(vertex2);
            if (!vertex1.neighbours.Contains(vertex3))
                vertex1.neighbours.Add(vertex3);

            if (!vertex2.neighbours.Contains(vertex1))
                vertex2.neighbours.Add(vertex1);
            if (!vertex2.neighbours.Contains(vertex3))
                vertex2.neighbours.Add(vertex3);

            if (!vertex3.neighbours.Contains(vertex1))
                vertex3.neighbours.Add(vertex1);
            if (!vertex3.neighbours.Contains(vertex2))
                vertex3.neighbours.Add(vertex2);
        }
    }

    /// <summary>
    /// Fonction permettant d'update les position min et max de la surface
    /// </summary>
    public void UpdateMinMax()
    {
        minX = minY = minZ = float.MaxValue;
        maxX = maxY = maxZ = float.MinValue;

        foreach (GameObject vertex in VerticesObjects)
        {
            if (vertex.transform.position.x > maxX) maxX = vertex.transform.position.x;
            if (vertex.transform.position.x < minX) minX = vertex.transform.position.x;
            if (vertex.transform.position.y > maxY) maxY = vertex.transform.position.y;
            if (vertex.transform.position.y < minY) minY = vertex.transform.position.y;
            if (vertex.transform.position.z > maxZ) maxZ = vertex.transform.position.z;
            if (vertex.transform.position.z < minZ) minZ = vertex.transform.position.z;
        }

        GetComponent<MeshRenderer>().material.SetVector("MinMaxPositions", new Vector2(minY, maxY)); 
    }

    /// <summary>
    /// Permet de rendre la surface avec des aretes dures et non arondies
    /// </summary>
    static void flattenFaces()
    {
        List<Vector3> vert = new List<Vector3>();
        List<int> tri = new List<int>();
        List<Color32> col32 = new List<Color32>();
        int i = 0;
        foreach (int t in triangles)
        {
            //Debug.Log("t = " + t + "\n vert size = " + vertices.Length + "\n col size = " + colors32.Length);
            vert.Add(vertices[t]);
            col32.Add(colors32[t]);
            tri.Add(i);
            i++;
        }

        flatVertices = vert.ToArray();
        flatTriangles = tri.ToArray();
        flatColors32 = col32.ToArray();
    }


    /// <summary>
    /// Permet d'update la mesh du renderer
    /// </summary>
    public static void UpdateMesh()
    {
        mesh.Clear();

        if (flatFaces)
        {
            flattenFaces();
            mesh.vertices = flatVertices;
            mesh.triangles = flatTriangles;
            mesh.colors32 = flatColors32;
        }
        else
        {
            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.colors32 = colors32;
        }

        mesh.RecalculateNormals();

        modified = true;
    }


    /// <summary>
    /// Permet de sauvegarder l'objet au format .obj
    /// </summary>
    /// <param name="path">
    /// Chemin de sauvegarde du fichier
    /// </param>
    public void saveObj(string path)
    {
        StreamWriter writer = new StreamWriter(path, false);
        CultureInfo myCI = CultureInfo.InvariantCulture;

        // vertices positions
        foreach (Vector3 vertex in vertices)
        {
            writer.WriteLine("v {0} {1} {2}", vertex[0].ToString(myCI), vertex[1].ToString(myCI), vertex[2].ToString(myCI));
        }
        writer.WriteLine(Environment.NewLine);

        // normals directions
    /*    foreach (Vector3 normal in mesh.normals)
        {
            writer.WriteLine("vn {0} {1} {2}", normal[0].ToString(myCI), normal[1].ToString(myCI), normal[2].ToString(myCI));
        }
        writer.WriteLine(Environment.NewLine);*/

        // faces
        for (int i = 0; i < triangles.Length; i=i+3)
        {
            writer.WriteLine("f {0}//{0} {1}//{1} {2}//{2}", triangles[i]+1, triangles[i+1]+1, triangles[i+2]+1);
        }
        writer.Close();

        modified = false;
    }


    /// <summary>
    /// Permet de charger un fichier .obj
    /// </summary>
    /// <param name="path">
    /// Chemein du fichier a charger
    /// </param>
    public void loadObj(string path)
    {
        StreamReader reader = new StreamReader(path);
        CultureInfo myCI = CultureInfo.InvariantCulture;
        List<Vector3> listVertices = new List<Vector3>();
        List<int> listTriangles = new List<int>();

        // read the whole file
        while (reader.Peek() >= 0)
        {
            string line = reader.ReadLine();
            char[] separators = { ' ' };
            string[] args = line.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            if (args.Length > 0 && !args[0].Equals('#'))
            {
                // vertices
                if (args[0] == "v")
                {
                    Vector3 vertex = new Vector3(
                        float.Parse(args[1], myCI),
                        float.Parse(args[2], myCI),
                        float.Parse(args[3], myCI)
                    );
                    listVertices.Add(vertex);
                }
                // triangles
                else if (args[0] == "f")
                {
                    if (args.Length != 4)
                    {
                        Debug.LogError("The mesh must be composed of triangles only");
                    }
                    else
                    {
                        for (int i = 1; i < 4; i++)
                        {
                            string[] vals = args[i].Split('/');
                            listTriangles.Add(int.Parse(vals[0]) - 1);
                        }
                    }
                }
            }
        }
        reader.Close();

        vertices = listVertices.ToArray();
        triangles = listTriangles.ToArray();

        // colors
        colors32 = new Color32[vertices.Length];
        for (int c = 0; c < colors32.Length; c++)
        {
            colors32[c] = new Color32(255, 255, 255, 255);
        }
    }

    /// <summary>
    /// Permet de changer le mode de rendu de l'objet
    /// </summary>
    /// <param name="mode">
    /// <list type="bullet">
    /// <item>
    /// <term>Default</term>
    /// <description>Rendu par défaut</description>
    /// </item>
    /// <item>
    /// <term>Wireframe</term>
    /// <description>Rendu wireframe de l'objet</description>
    /// </item>
    /// <item>
    /// <term>Combined</term>
    /// <description>Mode combiné, les aretes des triangles sont dessiné par dessus la mesh</description>
    /// </item>
    /// </list>
    /// </param>
    public void ChooseMaterial(string mode)
    {
        switch (mode)
        {
            case "Default":
                Debug.Log("Changement de mat : default");
                GetComponent<MeshRenderer>().materials = defaultMat;
                break;

            case "Wireframe":
                Debug.Log("Changement de mat : wireframe");
                GetComponent<MeshRenderer>().materials = wireMat;
                break;

            case "Combined":
                Debug.Log("Changement de mat : both");
                GetComponent<MeshRenderer>().materials = combinedMat;
                break;
        }
    }



    /// <summary>
    /// RPC permettanbt d'ajouter un vertex a l'objet, positionné entre 2 vertices deja existant
    /// </summary>
    /// <param name="vert1">
    /// 1er vertex existant
    /// </param>
    /// <param name="vert2">
    /// 2e vertex existant
    /// </param>
    [PunRPC]
    public void AddVertex(int vert1, int vert2)
    {
        // On recupere les coord des 2 vertices donnés
        Vector3 vertex1, vertex2;
        vertex1 = vertices[vert1];
        vertex2 = vertices[vert2];

        // le nouveau vertex est placé au milieu des 2 vertices
        Vector3 newVertex = (vertex1 + vertex2) / 2;

        // On ajoute le nouveau vertex au tableau de vertices de l'objet
        List<Vector3> meshVertices = new List<Vector3>(vertices);
        meshVertices.Add(newVertex);
        vertices = meshVertices.ToArray();

        //Réattribution des couleurs des vertices
        colors32 = new Color32[meshVertices.Count];
        for (int c = 0; c < colors32.Length; c++)
        {
            colors32[c] = new Color32(255, 255, 255, 255);
        }


        // -------------------- On va retriangulariser --------------------
        List<int> meshTriangles = new List<int>();
        List<int> currentTriangle = new List<int>(); //triangle courant

        // Create triangles
        for (int i = 0; i < triangles.Length; i += 3)
        {

            // Recuperation des vertices du triangle en cours
            currentTriangle.Clear();

            int a = triangles[i];
            int b = triangles[i + 1];
            int c = triangles[i + 2];

            currentTriangle.Add(a);
            currentTriangle.Add(b);
            currentTriangle.Add(c);


            // Si les 2 vertices donnés en param sont dans le triangle, alors on recupère le 3eme vertex du traingle et on separe le triangle en 2 :
            // le triangle est vert1->vert2->vert3 devient 2 triangle vert1->newVertex->vert3 et vert2->vert3->newVertex
            if (currentTriangle.Contains(vert1) && currentTriangle.Contains(vert2))
            {
                int thirdVert = 0;
                foreach (int vert in currentTriangle)
                {
                    if (!(vert1==vert || vert2==vert))
                    {
                        thirdVert = vert; // 3e vertex du triangle de base
                    }
                }

                // 1er triangle
                meshTriangles.Add(thirdVert);
                meshTriangles.Add(meshVertices.Count - 1);
                meshTriangles.Add(vert1);

                // 2e triangle
                meshTriangles.Add(thirdVert);
                meshTriangles.Add(meshVertices.Count - 1);
                meshTriangles.Add(vert2);

            }
            //Sinon le triangle reste  le meme
            else
            {
                meshTriangles.Add(triangles[i]);
                meshTriangles.Add(triangles[i + 1]);
                meshTriangles.Add(triangles[i + 2]);
            }
        }

        // Réattribution du tableau des triangles de la mesh
        triangles = meshTriangles.ToArray();

        // Creation de la sphere représentant le nouveau vertex
        AddVertexSphere(newVertex);

        // Update de la mesh
        UpdateMesh();

        // Update des voisins
        UpdateNeighbors();

    }


    /// <summary>
    /// RPC permettant de supprimer un ou plusieurs vertices à l'objet
    /// </summary>
    /// <param name="verts">
    /// Tableau des vertices à supprimer
    /// </param>
    [PunRPC]
    public void DeleteVertex(int[] verts)
    {
        List<GameObject> toDestroy = new List<GameObject>();
        List<GameObject> newVerticesObjects = new List<GameObject>();

        foreach (int index in verts)
        {
            toDestroy.Add(VerticesObjects[index]);
            VerticesObjects[index].GetComponent<NetworkVertexController>().FreeVertex();
        }

        List<int> meshTriangles = new List<int>();
        List<Vector3> meshVertices = new List<Vector3>();


        // Recherche des triangles défini par un des vertices donnés
        for (int i = 0; i < triangles.Length; i += 3)
        {
            // On ajoute a la nouvelle liste des triangle seulement ceux qui ne sont pas définie un des vertices donnés en param
            if (!(verts.Contains(triangles[i]) || verts.Contains(triangles[i + 1]) || verts.Contains(triangles[i + 2])))
            {
                meshTriangles.Add(triangles[i]);
                meshTriangles.Add(triangles[i + 1]);
                meshTriangles.Add(triangles[i + 2]);
            }
        }


        // On reassigne les bons indices de vertex dans le tableau des triangles
        for (int i = 0; i < meshTriangles.Count; i++)
        {
            int vertex = meshTriangles[i];
            int nb = 0;
            foreach (int selected in verts)
            {
                if (vertex > selected)
                {
                    nb++;
                }
            }
            meshTriangles[i] = vertex - nb;
        }


        // Suppresion des vertices
        for (int i = 0; i < vertices.Length; i++)
        {
            if (!verts.Contains(i))
            {
                meshVertices.Add(vertices[i]);
                newVerticesObjects.Add(VerticesObjects[i]);
            }
        }

        VerticesObjects = newVerticesObjects;


        // Reassign vertices & triangles to the object
        vertices = meshVertices.ToArray();
        triangles = meshTriangles.ToArray();
        colors32 = new Color32[meshVertices.Count];


        //Reattribution des bons indices sur les spheres
        for (int i = 0; i < vertices.Length; i++)
        {
            VerticesObjects[i].GetComponent<NetworkVertexController>().index = i;
        }

        // Reattribution des couleurs des vertices
        for (int c = 0; c < colors32.Length; c++)
        {
            colors32[c] = new Color32(255, 255, 255, 255);
        }


        // Update de la Mesh
        UpdateMesh();

        // Update des voisins
        UpdateNeighbors();

        // Suppression des spheres
        foreach (GameObject obj in toDestroy)
        {
            Destroy(obj);
        }
    }

    /// <summary>
    /// RPC permettant de créer un nouveau triangle entre 3 vertices
    /// </summary>
    /// <param name="vert1">
    /// 1er vertex
    /// </param>
    /// <param name="vert2">
    /// 2e vertex
    /// </param>
    /// <param name="vert3">
    /// 3e vertex
    /// </param>
    [PunRPC]
    public void CreateTriangle(int vert1, int vert2, int vert3)
    {

        List<int> meshTriangles = new List<int>(triangles);

        // Ajout du nouveau triangle    
        meshTriangles.Add(vert1);
        meshTriangles.Add(vert2);
        meshTriangles.Add(vert3);

        // Reattribution des triangles de la mesh
        triangles = meshTriangles.ToArray();

        // Update de la mesh
        UpdateMesh();

        // Update des voisins
        UpdateNeighbors();
    }

    /// <summary>
    /// RPC permettant de supprimer un triangle entre 3 vertices
    /// </summary>
    /// <param name="vert1">
    /// 1er vertex
    /// </param>
    /// <param name="vert2">
    /// 2e vertex
    /// </param>
    /// <param name="vert3">
    /// 3e vertex
    /// </param>
    [PunRPC]
    public void DeleteTriangle(int vert1, int vert2, int vert3)
    {
        List<int> tri = new List<int>() { vert1, vert2, vert3 };
        List<int> meshTriangles = new List<int>();

        // On cherche si un triangle existe avec les 3 vertices, si oui on ne l'ajoute pas a la nouvelle liste de triangle
        for (int i = 0; i < triangles.Length; i += 3)
        {
            if (!(tri.Contains(triangles[i]) && tri.Contains(triangles[i + 1]) && tri.Contains(triangles[i + 2])))
            {
                meshTriangles.Add(triangles[i]);
                meshTriangles.Add(triangles[i + 1]);
                meshTriangles.Add(triangles[i + 2]);
            }
        }

        // Reattribution des triangles de la mesh
        triangles = meshTriangles.ToArray();

        // Update de la mesh
        UpdateMesh();

        // Update des voisins
        UpdateNeighbors();
    }

    /// <summary>
    /// Permet de créer une nouvelle sphere pour représenter un vertex
    /// </summary>
    /// <param name="vertex">
    /// Le vertex concerné
    /// </param>
    public void AddVertexSphere(Vector3 vertex)
    {
        Debug.Log("Adding a vertex/sphere at " + vertex);
        GameObject sphere = Instantiate(vertexPrefab, transform.TransformPoint(vertex), transform.rotation);
        VerticesObjects.Add(sphere);
        sphere.transform.SetParent(this.transform);
        sphere.GetComponent<NetworkVertexController>().mainObject = this;
        sphere.GetComponent<NetworkVertexController>().index = vertices.Length-1;
    }

}
