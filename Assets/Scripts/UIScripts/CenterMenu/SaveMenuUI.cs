﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using TMPro;
using System.IO;

/// <summary>
/// Panneau d'export de l'objet réseau. Permet de choisir un nom de fichier de l'exporter au format obj.
/// </summary>
public class SaveMenuUI : MonoBehaviour
{
    /// <value>Clavier du système d'Oculus Quest</value>
    private TouchScreenKeyboard overlayKeyboard;

    /// <value>Panneau principal du Game Menu</value>
    [SerializeField] GameObject mainMenu;
    /// <value>Pop up de fichier déjà existant</value>
    [SerializeField] ReplaceFilePopUpUI replaceFilePopUp;

    /// <value>Champ de texte contenant le nom du fichier</value>
    public TMP_InputField fileName;
    /// <value>Chemin d'accès au fichier</value>
    public TMP_Text path;

    /// <value>Code indiquant si l'utilisateur souhaite rester connecté (0), quitter la salle réseau (1), ou fermer l'application (2)</value>
    public int shouldClose = 0; // 0 = don't close, 1 = leave room, 2 = quit app

    /// <summary>
    /// Initialisation du chemin d'accès et du nom du fichier en fonction des infos du Main Menu.
    /// </summary>
    private void Start()
    {
        path.text = MainMenuUI.path;
        string fname = MainMenuUI.fileName;
        
        if (!fname.Equals("New Shape"))
        {
            // remove extension
            fname = fname.Remove(fname.Length - 4);
            fileName.text = fname;
        }
    }

    /// <summary>
    /// Ouverture du pop up de fichier déjà existant si le nom choisi est déjà utilisé.
    /// Sinon, export du fichier obj et retour au menu principal.
    /// </summary>
    public void ExportObjBtn()
    {
        GameObject shape = GameObject.FindWithTag("NetworkObject");
        if (shape != null && fileName.text != "")
        {            
            string fullPath = path.text + fileName.text + ".obj";
            // check if file exists
            if (File.Exists(fullPath))
            {
                replaceFilePopUp.OpenPopUp(path.text, fileName.text);
            }
            else
            {
                // export to obj file
                SaveFile(fullPath);

                // back to game menu
                mainMenu.SetActive(true);
                gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Sauvegarde du fichier obj au chemin spécifié en paramètre.
    /// En fonction du code contenu dans la propriété shouldClose, quitte la salle ou ferme l'application.
    /// </summary>
    /// <param name="fullPath">Chemin d'accès comprenant le nom du fichier et l'extension</param>
    public void SaveFile(string fullPath)
    {
        GameObject.FindWithTag("NetworkObject").GetComponent<NetworkObject>().saveObj(fullPath);

        // check if user was leaving
        if (shouldClose != 0)
        {
            if (shouldClose == 1)
            {
                mainMenu.GetComponent<GameMenuUI>().LeaveRoom();
            }
            else if (shouldClose == 2)
            {
                mainMenu.GetComponent<GameMenuUI>().CloseApp();
            }
        }
    }

    /// <summary>
    /// Retourne au panneau principal du Game Menu. La sauvegarde est abandonnée.
    /// </summary>
    public void CancelBtn()
    {
        mainMenu.SetActive(true);
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Affiche le clavier système d'Oculus et récupère les valeurs saisies par l'utilisateur.
    /// </summary>
    /// <param name="field">Champ de texte utilisé</param>
    public void FocusKeyboard(TMP_InputField field)
    {
        overlayKeyboard = TouchScreenKeyboard.Open(field.text, TouchScreenKeyboardType.Default);
        if (overlayKeyboard != null)
        {
            field.text = overlayKeyboard.text;
        }
    }

    /// <summary>
    /// Ferme le clavier système d'Oculus.
    /// </summary>
    public void UnfocusKeyboard()
    {
        overlayKeyboard.active = false;
    }
}
