﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Affiche et teste la réussite des différents challenges.
/// </summary>
public class ChallengeUI : MonoBehaviour
{
    /// <value>Panneau d'affichage du résultat.</value>
    [SerializeField] ResultChallengeUI resultUI;
    /// <value>Image du challenge en cours.</value>
    [SerializeField] RawImage activeImage;
    /// <value>Numéro du challenge en cours.</value>
    [SerializeField] int ChallengeNumber = 0;
    /// <value>Tableau d'images de tous les challenges.</value>
    [SerializeField] Texture[] challengeImages;

    /// <summary>
    /// Affichage du premier challenge à l'initialisation.
    /// </summary>
    void Start()
    {
        if (challengeImages.Length > 0)
            activeImage.texture = challengeImages[ChallengeNumber];
    }

    /// <summary>
    /// Affichage du challenge précédent.
    /// </summary>
    public void previousBtn()
    {
        ChallengeNumber = ChallengeNumber - 1;
        // si le challenge actuel est le premier de la liste, affichage du dernier challenge.
        if (ChallengeNumber < 0) ChallengeNumber = challengeImages.Length - 1;
        activeImage.texture = challengeImages[ChallengeNumber];
    }

    /// <summary>
    /// Affichage du challenge suivant.
    /// </summary>
    public void nextBtn()
    {
        // si le challenge actuel est le dernier de la liste, affichage du premier challenge.
        ChallengeNumber = (ChallengeNumber + 1) % challengeImages.Length;
        activeImage.texture = challengeImages[ChallengeNumber];
    }

    /// <summary>
    /// Choix de la fonction de test à utiliser en fonction du numéro de challenge et affichage du résultat.
    /// </summary>
    public void testShapeBtn()
    {
        // test de l'objet et fonction du challenge
        switch (ChallengeNumber)
        {
            case 0:
                testFlat();
                break;
            default:
                break;
        }

        // affichage du panneau de résultat
        gameObject.SetActive(false);
        resultUI.gameObject.SetActive(true);
    }

    /// <summary>
    /// Test de l'objet pour le challenge consistant à obtenir une surface horizontale plate.
    /// Affectation des message de succès ou d'échec en fonction du résultat.
    /// </summary>
    void testFlat()
    {
        // Tolérance de 10 cm entre le point le plus haut et le point le plus bas.
        float delta = 0.1f;

        GameObject shape = GameObject.FindGameObjectWithTag("NetworkObject");
        if (shape != null)
        {
            // Succès
            if ((shape.GetComponent<NetworkObject>().maxY - shape.GetComponent<NetworkObject>().minY) < delta)
            {
                resultUI.message.text = "Congratulations! You fulfilled the challenge!";
            }
            // Echec
            else
            {
                resultUI.message.text = "Your shape isn't flat enough. Try again using the elevation color mode.";
            }
        }
    }
}
