﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using TMPro;
using Photon.Pun;

/// <summary>
/// Gère l'affichage des informations de la room.
/// </summary>
public class RoomInfoUI : MonoBehaviour
{
    /// <value>Champ de texte contenant le nom de la salle.</value>
    [SerializeField] TMP_Text roomName;
    /// <value>Champ de texte contenant le nombre de joueurs dans la salle.</value>
    [SerializeField] TMP_Text roomPlayers;

    /// <value>Nombre de joueurs présents dans la salle.</value>
    private int nbPlayers;
    /// <value>Nombre maximum de joueurs pouvant entrer dans la salle.</value>
    private int maxPlayers;

    /// <summary>
    /// Initialisation du nom de la salle et du nombre de joueurs maximum.
    /// </summary>
    private void Awake()
    {
        roomName.text = PhotonNetwork.CurrentRoom.Name;
        maxPlayers = PhotonNetwork.CurrentRoom.MaxPlayers;
    }

    /// <summary>
    /// Mise à jour du nombre de joueurs actuel, à chaque ouverture du menu.
    /// </summary>
    private void OnEnable()
    {
        nbPlayers = PhotonNetwork.CurrentRoom.PlayerCount;

        roomPlayers.text = nbPlayers + "/" + maxPlayers;
    }
}
