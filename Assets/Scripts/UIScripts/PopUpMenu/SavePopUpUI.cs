﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;

/// <summary>
/// Gère fonctions des boutons du pop up de sauvegarde avant de quitter.
/// </summary>
public class SavePopUpUI : MonoBehaviour
{
    /// <value>Panneau principal du Game Menu</value>
    [SerializeField] GameObject gameMenu;
    /// <value>Panneau de sauvegarde du Game Menu</value>
    [SerializeField] GameObject saveMenu;

    /// <value>Vaut vrai si le pop up s'est affiché après un click sur le bouton Quit App</value>
    public bool closeApp = false;

    /// <summary>
    /// Ouvre le panneau de sauvegarde et ferme le pop up.
    /// Indique au menu de sauvegarde s'il faut quitter la salle ou fermer l'application à la fin de l'export.
    /// </summary>
    public void SaveBtn()
    {
        gameMenu.SetActive(false);
        saveMenu.SetActive(true);
        saveMenu.GetComponent<SaveMenuUI>().shouldClose = closeApp ? 2 : 1;
        ClosePopUp();
    }

    /// <summary>
    /// Quitte la salle ou ferme l'application, en fonction du bouton cliqué précédemment.
    /// </summary>
    public void DontSaveBtn()
    {
        if (closeApp)
        {
            gameMenu.GetComponent<GameMenuUI>().CloseApp();
        }
        else
        {
            gameMenu.GetComponent<GameMenuUI>().LeaveRoom();
        }
    }

    /// <summary>
    /// Ferme le pop up. L'utilisateur reste dans le salle.
    /// </summary>
    public void CancelBtn()
    {
        ClosePopUp();
    }

    /// <summary>
    /// Désactive le panneau et son canvas.
    /// </summary>
    void ClosePopUp()
    {
        transform.parent.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Active le panneau et son canvas.
    /// </summary>
    public void OpenPopUp()
    {
        transform.parent.gameObject.SetActive(true);
        gameObject.SetActive(true);
    }
}
