﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using System.Collections.Generic;


/// <summary>
/// Classe gérant les Vertices représentés par des sphères 
/// </summary>
public class NetworkVertexController : XRGrabInteractable
{
    /// <value> Indice du vertex auquel se réfère la sphère </value>
    public int index;

    /// <value> Liste des voisins de ce vertex </value>
    public List<NetworkVertexController> neighbours = new List<NetworkVertexController>();

    /// <value> Objet résau auquel appartient le vertex </value>
    public NetworkObject mainObject;

    /// <value> Objet parent </value>
    private GameObject parent;

    /// <value> Couleur de vertex bloqué </value>
    public Color blockedVertex;
    /// <value> Couleur de vertex débloqué </value>
    public Color unblockedVertex;
    /// <value> Couleur de vertex sélectionné </value>
    public Color selectedVertex;

    /// <value> Couleur de vertex hover </value>
    [SerializeField] Color hoveredVertex;
    /// <value> Valeur d'émission </value>
    [SerializeField] float hoveredEmissionValue;

    /// <value> Couleur de modification de la mesh </value>
    [SerializeField] Color32 modifiedMesh;
    /// <value> Couleur par défaut de la mesh </value>
    [SerializeField] Color32 defaultMesh;

    /// <value> Vaut vrai si le vertex est dans la liste de sélection </value>
    public bool isInSelectedList;

    /// <summary>
    /// Initialisation du vertex et attribution du parent.
    /// </summary>
    void Start()
    {
        mainObject = GameObject.FindGameObjectWithTag("NetworkObject").GetComponent<NetworkObject>();
        parent = transform.parent.gameObject;
        GetComponent<Renderer>().material.SetColor("BaseColor", unblockedVertex);
        GetComponent<Renderer>().material.SetFloat("EmitLight", 0.0f);
    }

    /// <summary>
    /// Si le vertex est attrapé (grab), on synchronise sa position.
    /// </summary>
    private void Update()
    {
        if (isSelected)
        {
            mainObject.UpdateVertexPosition(index, parent.transform.InverseTransformPoint(transform.position));
        }
    }

    /// <summary>
    /// Callback fournie par XRGrabInteractable, appellée lorsque l'on appuie sur le bouton de grab.
    /// Sélection du vertex et blocage pour les autres joueurs.
    /// </summary>
    /// <param name="interactor"></param>
    protected override void OnSelectEntered(XRBaseInteractor interactor)
    {
        base.OnSelectEntered(interactor);

        // Ajout du vertex dans la selection
        SelectionManager.selectVertex(this.gameObject);
        // Blocage du vertex et de ses voisins pour les autres joueurs
        mainObject.BlockVertex(index);
        //Changement de couleur de la mesh autour du vertex
        NetworkObject.colors32[index] = modifiedMesh;
    }


    /// <summary>
    /// Callback fournie par XRGrabInteractable, appellée lorsque l'on relache le bouton de grab.
    /// Désélection du vertex, déblocage pour les autres joueurs et mise à jour de la position finale.
    /// </summary>
    /// <param name="interactor"></param>
    protected override void OnSelectExited(XRBaseInteractor interactor)
    {
        base.OnSelectExited(interactor);

        // Suppression du vertex dans la selection
        SelectionManager.deselectVertex(this.gameObject);
        // Déblocage du vertex et de ses voisins pour les autres joueurs
        mainObject.UnblockVertex(index);
        // Couleur par défaut du vertex
        NetworkObject.colors32[index] = defaultMesh;

        // update final vertex position
        mainObject.UpdateVertexPosition(index, parent.transform.InverseTransformPoint(transform.position));
        NetworkObject.UpdateMesh();
    }


    /// <summary>
    /// Callback fournie par XRGrabInteractable, appellée lorsque le rayon ou notre main s'approche de l'objet.
    /// </summary>
    /// <param name="interactor"></param>
    protected override void OnHoverEntered(XRBaseInteractor interactor)
    {
        base.OnHoverEntered(interactor);
        // S'il n'est pas dans la liste de sélection, on change sa couleur pour une couleur émissive
        if (!isInSelectedList)
        {
            GetComponent<Renderer>().material.SetColor("BaseColor", hoveredVertex);
            GetComponent<Renderer>().material.SetFloat("EmitLight", 1.0f);
        }
    }


    /// <summary>
    /// Callback fournie par XRGrabInteractable, appellée lorsque le rayon ou notre main s'éloigne de l'objet.
    /// </summary>
    /// <param name="interactor"></param>
    protected override void OnHoverExited(XRBaseInteractor interactor)
    {
        base.OnHoverExited(interactor);
        // S'il n'est pas dans la liste de sélection, on change sa couleur pour une couleur par defaut
        if (!isInSelectedList)
        {
            GetComponent<Renderer>().material.SetColor("BaseColor", unblockedVertex);
            GetComponent<Renderer>().material.SetFloat("EmitLight", 0.0f);
        }
    }


    /// <summary>
    /// Permet de débloquer le vertex assigné.
    /// </summary>
    public void FreeVertex()
    {
        isInSelectedList = false;
        GetComponent<Renderer>().material.SetColor("BaseColor", unblockedVertex);
        GetComponent<Renderer>().material.SetFloat("EmitLight", 0.0f);
        mainObject.UnblockVertex(index);
    }
}
