﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;


/// <summary>
/// Manager pour gérer les connexions au serveur, les créations de salles et les tentatives de connexion à d'autres salles.
/// </summary>
public class NetworkManager : MonoBehaviourPunCallbacks
{
    /// <value> Nom de la salle </value>
    static string roomName;

    /// <value> Options de la salle </value>
    static RoomOptions roomOptions;

    /// <value> Message d'erreur rencontré </value>
    static string errorMessage;


    /// <summary>
    /// Start du gameobject associé.
    /// Mise en place d'un Singleton pour éviter les conflits.
    /// </summary>
    void Start()
    {
        // only one game manager
        GameObject[] networkManagers = GameObject.FindGameObjectsWithTag("NetworkManager");
        if (networkManagers.Length > 1)
        {
            Destroy(this.gameObject);
        }

        // connection au serveur
        if (!PhotonNetwork.IsConnected)
        {
            ConnectToServer();
        }
        DontDestroyOnLoad(this.gameObject);
    }

    /// <summary>
    /// Callback photon quand on est connecté au serveur.
    /// </summary>
    private void ConnectToServer()
    {
        PhotonNetwork.ConnectUsingSettings();
        Debug.Log("Try Connect To Server...");
    }

    /// <summary>
    /// Callback photon
    /// </summary>
    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected To Server.");
        base.OnConnectedToMaster();
    }

    /// <summary>
    /// Callabck quand la scene a fini de charger
    /// </summary>
    /// <param name="scene"></param>
    /// <param name="mode"></param>
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(PhotonNetwork.IsMasterClient)
            PhotonNetwork.InstantiateRoomObject("Network Object", transform.position, transform.rotation);
    }

    /// <summary>
    /// Fonction chargant la scene multi et créant la room photon
    /// </summary>
    /// <param name="name">
    /// Le nom de la room photon a créer
    /// </param>
    /// <param name="options">
    /// Les options de la room
    /// </param>
    public static void LoadAndCreateRoom(string name, RoomOptions options)
    {
        roomName = name;
        roomOptions = options;
        SceneManager.LoadScene("NetworkTestScene");
        SceneManager.sceneLoaded += OnCreatedSceneLoaded;
    }

    /// <summary>
    /// Callaback quand la scene multi a fini de charger
    /// </summary>
    /// <param name="scene"></param>
    /// <param name="mode"></param>
    static void OnCreatedSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        SceneManager.sceneLoaded -= OnCreatedSceneLoaded;
        PhotonNetwork.CreateRoom(roomName, roomOptions, TypedLobby.Default);
    }


    /// <summary>
    /// Callabck quand la création de la room est réussi (fourni par photon)
    /// </summary>
    public override void OnCreatedRoom()
    {
        Debug.Log("Room Creation Succeeded");
        base.OnCreatedRoom();

        if (PhotonNetwork.IsMasterClient)
            PhotonNetwork.InstantiateRoomObject("Network Object", transform.position, transform.rotation);
    }

    /// <summary>
    /// Fonction chargant la scene multi et rejoignant la room photon
    /// </summary>
    /// <param name="name">
    /// Le nom de la room photon à rejoindre
    /// </param>
    public static void LoadAndJoinRoom(string name)
    {
        roomName = name;
        SceneManager.LoadScene("NetworkTestScene");
        SceneManager.sceneLoaded += OnJoinedSceneLoaded;
    }

    /// <summary>
    /// Callabck quand la scene a fini de charger
    /// </summary>
    /// <param name="scene"></param>
    /// <param name="mode"></param>
    static void OnJoinedSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        SceneManager.sceneLoaded -= OnJoinedSceneLoaded;
        PhotonNetwork.JoinRoom(roomName);
    }

    /// <summary>
    /// Callback quand l'on rejoint une room (fourni par Photon)
    /// </summary>
    public override void OnJoinedRoom()
    {
        Debug.Log("Joined a Room.");
        errorMessage = "";
        base.OnJoinedRoom();
    }


    /// <summary>
    /// Callback quand l'on quitte une room (fourni par Photon)
    /// </summary>
    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        SceneManager.LoadScene("MainMenu");
    }


    /// <summary>
    /// Callback quand une erreur est survenue lors de la connexion à une salle (fourni par Photon)
    /// </summary>
    /// <param name="returnCode">
    /// Code de retour
    /// </param>
    /// <param name="message">
    /// Message d'erreur
    /// </param>
    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.LogError("Failed to join the room ! \n Reason : " + message);
        errorMessage = "Failed to join the room ! \n Reason : " + message;
        SceneManager.sceneLoaded += OnFailed;
        SceneManager.LoadScene("ErrorScene");
        base.OnJoinRoomFailed(returnCode, message);
    }

    /// <summary>
    /// Callback quand une erreur est survenue lors de la création d'une salle (fourni par Photon)
    /// </summary>
    /// <param name="returnCode">
    /// Code de retour
    /// </param>
    /// <param name="message">
    /// Message d'erreur
    /// </param>
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.LogError("Failed to create the room ! \n Reason : " + message);
        errorMessage = "Failed to create the room ! \n Reason : " + message;
        SceneManager.sceneLoaded += OnFailed;
        SceneManager.LoadScene("ErrorScene");
        base.OnCreateRoomFailed(returnCode, message);
    }

    /// <summary>
    /// Callback quand une erreur est survenue lors de la création ou la connexion à une salle et que la scene d'erreur a fini de charger
    /// </summary>
    /// <param name="scene"></param>
    /// <param name="mode"></param>
    static void OnFailed(Scene scene, LoadSceneMode mode)
    {
        SceneManager.sceneLoaded -= OnFailed;
        ErrorSceneMenuScript.messageBox.text = errorMessage;
    }

    /// <summary>
    /// Callback quand un autre joueur rejoint notre salle
    /// </summary>
    /// <param name="newPlayer">
    /// le nouveau joueur
    /// </param>
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("A new player joined the room.");
        base.OnPlayerEnteredRoom(newPlayer);
    }
}
