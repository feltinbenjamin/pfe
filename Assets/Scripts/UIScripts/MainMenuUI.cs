﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using System.Collections.Generic;
using System.IO;
using UnityEngine;
using TMPro;
using Photon.Realtime;

#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

/// <summary>
/// Gere l'UI du menu principal
/// </summary>
public class MainMenuUI : MonoBehaviour
{
    /// <value> Overlay Clavier Oculus </value>
    private TouchScreenKeyboard overlayKeyboard;

    /// <value> Pseudo rentré </value>
    public static string ChosenUsername;

    /// <value> Chemin d'acces aux fichiers </value>
    public static string path;
    /// <value> Nom du fichier </value>
    public static string fileName;

    /// <value> Menu de base </value>
    [SerializeField] GameObject mainPanel;
    /// <value> Menu de création </value>
    [SerializeField] GameObject createPanel;
    /// <value> Menu de connexion </value>
    [SerializeField] GameObject joinPanel;

    /// <value> Nom de la room créée </value>
    [SerializeField] TMP_InputField createRoomName;
    /// <value> Nombre max de joueur dans la room </value>
    [SerializeField] TMP_InputField createMaxPlayers;
    /// <value> Pseudo pour créer </value>
    [SerializeField] TMP_InputField createUsername;
    /// <value> Nom de la room a rejoindre </value>
    [SerializeField] TMP_InputField joinRoomName;
    /// <value> Pseudo pour rejoindre </value>
    [SerializeField] TMP_InputField joinUsername;

    /// <value> Menu deroulant des fichiers .obj trouvés </value>
    [SerializeField] TMP_Dropdown dropdownFiles;



    /// <summary>
    /// Start du GameObject
    /// </summary>
    void Start()
    {
        // Si on est sur Android (ie. Build Oculus), on demande l'acces au Micro
        #if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            Permission.RequestUserPermission(Permission.Microphone);
        }
        #endif


        // Init des differents menus
        mainPanel.SetActive(true);
        createPanel.SetActive(false);
        joinPanel.SetActive(false);

        List<string> dropOptions = new List<string> { "New Shape" };

        // get a list of all obj files in persistentDataPath
        path = Application.persistentDataPath + "/";
        string[] fileEntries = Directory.GetFiles(path, "*.obj");
        foreach (string fileName in fileEntries)
        {
            dropOptions.Add(Path.GetFileName(fileName));
        }

        dropdownFiles.ClearOptions();
        dropdownFiles.AddOptions(dropOptions);
    }


    /// <summary>
    /// Affiche le menu de base
    /// </summary>
    public void OpenMainMenu()
    {
        mainPanel.SetActive(true);
        createPanel.SetActive(false);
        joinPanel.SetActive(false);
    }

    /// <summary>
    /// Affiche le menu de création
    /// </summary>
    public void OpenCreateMenu()
    {
        mainPanel.SetActive(false);
        createPanel.SetActive(true);
        joinPanel.SetActive(false);
    }

    /// <summary>
    /// Affiche le menu de connexion
    /// </summary>
    public void OpenJoinMenu()
    {
        mainPanel.SetActive(false);
        createPanel.SetActive(false);
        joinPanel.SetActive(true);
    }

    /// <summary>
    /// Quitte l'application
    /// </summary>
    public void QuitBtn()
    {
        Application.Quit();
    }

    /// <summary>
    /// Prepare la création de la room par le NetworkManager
    /// </summary>
    public void CreateBtn()
    {
        ChosenUsername = createUsername.text;

        byte.TryParse(createMaxPlayers.text, out byte maxPlayers);

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = maxPlayers;
        roomOptions.IsVisible = true;
        roomOptions.IsOpen = true;

        string roomName = createRoomName.text;

        if(roomName != "" && maxPlayers > 0 && ChosenUsername != "")
        {
            fileName = dropdownFiles.options[dropdownFiles.value].text;
            NetworkManager.LoadAndCreateRoom(roomName, roomOptions);
        }
    }

    /// <summary>
    /// Prepare la connexion a la room par le NetworkManager
    /// </summary>
    public void JoinBtn()
    {
        ChosenUsername = joinUsername.text;

        string roomName = joinRoomName.text;

        if (roomName != "" && ChosenUsername != "")
        {
            NetworkManager.LoadAndJoinRoom(roomName);
        }
    }

    /// <summary>
    /// Affiche le clavier Oculus
    /// </summary>
    /// <param name="field">
    /// Le champ d'entrée de texte lié
    /// </param>
    public void FocusKeyboard(TMP_InputField field)
    {
        overlayKeyboard = TouchScreenKeyboard.Open(field.text, TouchScreenKeyboardType.Default);
        if (overlayKeyboard != null)
        {
            field.text = overlayKeyboard.text;
        }
    }


    /// <summary>
    /// Supprime l'affichage du clavier Oculus
    /// </summary>
    public void UnfocusKeyboard()
    {
        overlayKeyboard.active = false;
    }



}
