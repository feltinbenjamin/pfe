var indexSectionsWithContent =
{
  0: "abcdefghijlmnopqrstuvwxyz",
  1: "ceghlmnoprs",
  2: "abcdefghijlmnopqrstuv",
  3: "abcdefghijlmnoprstuvwxz",
  4: "r",
  5: "xyz"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "functions",
  3: "variables",
  4: "enums",
  5: "enumvalues"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Fonctions",
  3: "Variables",
  4: "Énumérations",
  5: "Valeurs énumérées"
};

