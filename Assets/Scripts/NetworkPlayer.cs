﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using UnityEngine;
using UnityEngine.XR;
using Photon.Pun;
using UnityEngine.XR.Interaction.Toolkit;
using TMPro;


/// <summary>
/// Gère les avatars réseau
/// </summary>
public class NetworkPlayer : MonoBehaviour
{
    /// <value> Transform Component du pseudo </value>
    public Transform username;
    /// <value> Transform Component de la tete </value>
    public Transform head;
    /// <value> Transform Component de la main gauche </value>
    public Transform leftHand;
    /// <value> Transform Component de la main droite </value>
    public Transform rightHand;

    /// <value> PhotonView Component de l'objet </value>
    public PhotonView photonView;

    /// <value> Rayon de la main gauche de l'avatar </value>
    [SerializeField] GameObject leftRaylight;
    /// <value> Rayon de la main droite de l'avatar </value>
    [SerializeField] GameObject rightRaylight;

    /// <value> Hand Menu de l'avatar </value>
    [SerializeField] GameObject handMenu;
    /// <value> Game Menu de l'avatar </value>
    [SerializeField] GameObject gameMenu;

    /// <value> différence de teinte de la couleur du rayon entre la couleur de base et la couleur de hover </value>
    [SerializeField] float raylightHueDifference = 0.6f;

    /// <value> Animator Component de la main gauche </value>
    public Animator leftHandAnimator;
    /// <value> Animator Component de la main droite </value>
    public Animator rightHandAnimator;

    /// <value> XRRig Compoennt de l'objet </value>
    private XRRig rig;
    /// <value> Transform Component du casque </value>
    private Transform headRig;
    /// <value> Transform Component de la manette gauche </value>
    private Transform leftHandRig;
    /// <value> Transform Component du la manette droite </value>
    private Transform rightHandRig;

    /// <value> Couleur de l'avatar </value>
    public Color color;

    /// <summary>
    /// Awake du gameobject
    /// </summary>
    void Awake()
    {
        photonView = GetComponent<PhotonView>();

        XRRig rig = FindObjectOfType<XRRig>();
        headRig = rig.transform.Find("CameraOffset/VR Camera");
        leftHandRig = rig.transform.Find("CameraOffset/Left Controller/Left Hand");
        rightHandRig = rig.transform.Find("CameraOffset/Right Controller/Right Hand");

        // Initialisation de notre avatar
        if (photonView.IsMine)
        {
            rig.GetComponent<PlayerControler>().setNetworkPlayer(this);

            // initialise name and color for others
            photonView.RPC("SetUsername", RpcTarget.AllBuffered, MainMenuUI.ChosenUsername, photonView.ViewID);
            color = NetworkPlayerSpawner.myColor;
            photonView.RPC("SetColor", RpcTarget.AllBuffered, color.r, color.g, color.b, photonView.ViewID);

            // Local avatar's color
            Color diffColor = new Color(raylightHueDifference / 2.0f, raylightHueDifference / 2.0f, raylightHueDifference / 2.0f);
            Color validRaylightColor = color + diffColor; 
            Color invalidRaylightColor = color - diffColor;
            foreach (Renderer renderer in leftHandRig.GetComponentsInChildren<Renderer>())
            {
                renderer.material.SetColor("_BaseColor", color);
                renderer.material.SetColor("_SpecColor", color);
            }
            foreach (XRInteractorLineVisual line in leftHandRig.parent.GetComponentsInChildren<XRInteractorLineVisual>())
            {
                line.validColorGradient.colorKeys = new[] { new GradientColorKey(validRaylightColor, 0f), new GradientColorKey(validRaylightColor, 1f) };
                line.invalidColorGradient.colorKeys = new[] { new GradientColorKey(invalidRaylightColor, 0f), new GradientColorKey(invalidRaylightColor, 1f) };
            }
            foreach (Renderer renderer in rightHandRig.GetComponentsInChildren<Renderer>())
            {
                renderer.material.SetColor("_BaseColor", color);
                renderer.material.SetColor("_SpecColor", color);
            }
            foreach (XRInteractorLineVisual line in rightHandRig.parent.GetComponentsInChildren<XRInteractorLineVisual>())
            {
                line.validColorGradient.colorKeys = new[] { new GradientColorKey(validRaylightColor, 0f), new GradientColorKey(validRaylightColor, 1f) };
                line.invalidColorGradient.colorKeys = new[] { new GradientColorKey(invalidRaylightColor, 0f), new GradientColorKey(invalidRaylightColor, 1f) };
            }

            // rig's initial position
            rig.transform.position = this.transform.position;

            // Disabling the rendering of network avatar if it is us
            foreach (Renderer item in GetComponentsInChildren<Renderer>(true))
            {
                item.enabled = false;
            }
            leftRaylight.SetActive(false);
            rightRaylight.SetActive(false);
        }

        // activate hand menu at start
        handMenu.SetActive(true);
    }


    /// <summary>
    /// RPC pour synchroniser le pseudo du joueur
    /// </summary>
    /// <param name="newName">
    /// Pseudo du joueur
    /// </param>
    /// <param name="viewID">
    /// son PhotonViewID (identifiant unique sur le reseau)
    /// </param>
    [PunRPC]
    void SetUsername(string newName, int viewID)
    {
        PhotonView.Find(viewID).gameObject.GetComponentInChildren<TMP_Text>().text = newName;
    }


    /// <summary>
    /// RPC pour synchroniser la couleur du joueur
    /// </summary>
    /// <param name="r">
    /// Couleur rouge
    /// </param>
    /// <param name="g">
    /// Couelur verte
    /// </param>
    /// <param name="b">
    /// Couleur bleu
    /// </param>
    /// <param name="viewID">
    /// PhotonViewID (identifiant unique sur le reseau) du joueur
    /// </param>
    [PunRPC]
    void SetColor(float r, float g, float b, int viewID)
    {
        //Changement de couleur de tous les elements fils de l'objet (mains, tete, corps)
        foreach (Renderer renderer in PhotonView.Find(viewID).GetComponentsInChildren<Renderer>())
        {
            if (renderer.material.HasProperty("_BaseColor"))
            {
                renderer.material.SetColor("_BaseColor", new Color(r, g, b, renderer.material.GetColor("_BaseColor").a));
            }
            if (renderer.material.HasProperty("_SpecColor"))
            {
                renderer.material.SetColor("_SpecColor", new Color(r, g, b, renderer.material.GetColor("_SpecColor").a));
            }
        }

        // Changmement de couleur des rayons
        foreach (XRInteractorLineVisual line in PhotonView.Find(viewID).GetComponentsInChildren<XRInteractorLineVisual>())
        {
            line.validColorGradient.colorKeys = new[] { new GradientColorKey(new Color(r, g, b), 0f), new GradientColorKey(new Color(r, g, b), 1f) };
            line.invalidColorGradient.colorKeys = new[] { new GradientColorKey(new Color(r, g, b), 0f), new GradientColorKey(new Color(r, g, b), 1f) };
        }
    }


    /// <summary>
    /// RPC pour synchroniser la visibilité des rayons
    /// </summary>
    /// <param name="left">
    /// boolen valant vrai s'il s'agit du rayon de gauche, faux s'il s'agit de celui de droite
    /// </param>
    /// <param name="activate">
    /// booleen valant vrai si le rayon est visible
    /// </param>
    /// <param name="viewID">
    /// PhotonViewID (identifiant unique sur le reseau) du joueur
    /// </param>
    [PunRPC]
    void SetActivationRaylight(bool left, bool activate, int viewID)
    {
        NetworkPlayer avatar = PhotonView.Find(viewID).GetComponent<NetworkPlayer>();
        if (left)
        {
            avatar.leftRaylight.SetActive(activate);
        }
        else
        {
            avatar.rightRaylight.SetActive(activate);
        }
    }

    /// <summary>
    /// Prepare la RPC de synchronisation de la visibilité des rayons de notre joueur
    /// </summary>
    /// <param name="left">
    /// boolen valant vrai s'il s'agit du rayon de gauche, faux s'il s'agit de celui de droite
    /// </param>
    /// <param name="activate">
    /// booleen valant vrai si le rayon est visible
    /// </param>
    public void SyncRaylight(bool left, bool activate)
    {
        photonView.RPC("SetActivationRaylight", RpcTarget.Others, left, activate, photonView.ViewID);
    }


    /// <summary>
    /// RPC pour synchroniser la visibilité du Hand Menu
    /// </summary>
    /// <param name="activate">
    /// booleen valant vrai si le menu est visible
    /// </param>
    /// <param name="viewID">
    /// PhotonViewID (identifiant unique sur le réseau) du joueur
    /// </param>
    [PunRPC]
    void SetActivationHandMenu(bool activate, int viewID)
    {
        NetworkPlayer avatar = PhotonView.Find(viewID).GetComponent<NetworkPlayer>();
        avatar.handMenu.SetActive(activate);
    }

    /// <summary>
    /// Prepare la RPC de synchronisation de la visibilité du Hand Menu de notre joueur
    /// </summary>
    /// <param name="activate">
    /// booleen valant vrai si le menu est visible
    /// </param>
    public void SyncHandMenu(bool activate)
    {
        // activate for all to have collider active for the owner too
        photonView.RPC("SetActivationHandMenu", RpcTarget.All, activate, photonView.ViewID);
    }

    /// <summary>
    /// RPC pour synchroniser la visibilité du Game Menu
    /// </summary>
    /// <param name="activate">
    /// booleen valant vrai si le menu est visible
    /// </param>
    /// <param name="viewID">
    /// PhotonViewID (identifiant unique sur le reseau) du joueur
    /// </param>
    [PunRPC]
    void SetActivationGameMenu(bool activate, int viewID)
    {
        NetworkPlayer avatar = PhotonView.Find(viewID).GetComponent<NetworkPlayer>();
        avatar.gameMenu.SetActive(activate);
    }

    /// <summary>
    /// Prepare la RPC de synchronisation de la visibilité du Game Menu de notre joueur
    /// </summary>
    /// <param name="activate">
    /// booleen valant vrai si le menu est visible
    /// </param>
    public void SyncGameMenu(bool activate)
    {
        photonView.RPC("SetActivationGameMenu", RpcTarget.Others, activate, photonView.ViewID);
    }

    /// <summary>
    /// Update du gameobject. On synchronise a chaque frame la position et l'animation des main de notre joueur
    /// </summary>
    void Update()
    {

        if (photonView.IsMine)
        {
            // On fait correspondre le casque avec la tete de l'avatar
            MapPosition(head, headRig);
            // On fait correspondre la manette gauche avec la main gauche de l'avatar
            MapPosition(leftHand, leftHandRig);
            // On fait correspondre la manette droite avec la main droite de l'avatar
            MapPosition(rightHand, rightHandRig);

            // Update des animations des mains
            UpdateHandAnimation(InputDevices.GetDeviceAtXRNode(XRNode.LeftHand), leftHandAnimator);
            UpdateHandAnimation(InputDevices.GetDeviceAtXRNode(XRNode.RightHand), rightHandAnimator);
        }
        else
        {
            username.LookAt(headRig);
            username.forward = -username.forward;
        }

    }

    /// <summary>
    /// Animation des mains selon les touches appuyées
    /// </summary>
    /// <param name="targetDevice">
    /// Le device / manette gauche ou droit
    /// </param>
    /// <param name="handAnimator">
    /// l'animator component de la main
    /// </param>
    void UpdateHandAnimation(InputDevice targetDevice, Animator handAnimator)
    {
        if (targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue))
        {
            handAnimator.SetFloat("Trigger", triggerValue);
        }
        else
        {
            handAnimator.SetFloat("Trigger", 0);
        }

        if (targetDevice.TryGetFeatureValue(CommonUsages.grip, out float gripValue))
        {
            handAnimator.SetFloat("Grip", gripValue);
        }
        else
        {
            handAnimator.SetFloat("Grip", 0);
        }
    }

    /// <summary>
    /// Envoie de nos mouvements sur le réseau (via Photon)
    /// </summary>
    /// <param name="target">
    /// composant de l'avatar réseau (tete / mains)
    /// </param>
    /// <param name="rigTransform">
    /// materiel tracké (casque / manettes)
    /// </param>
    void MapPosition(Transform target, Transform rigTransform)
    {
        target.position = rigTransform.position;
        target.rotation = rigTransform.rotation;
    }
}
