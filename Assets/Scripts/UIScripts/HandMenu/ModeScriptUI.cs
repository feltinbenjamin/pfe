﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using Photon.Pun;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// Menu situé sur la main gauche du joueur.
/// Permet de selectionner l'action que l'on veut effectuer sur les vertices selectionnés
/// </summary>
public class ModeScriptUI : MonoBehaviour
{

    /// <value> Bouton activant le menu d'action </value>
    [SerializeField] Button modeAndActionsBtn;
    /// <value> Bouton activant le menu des options de l'objet </value>
    [SerializeField] Button shapeOptionsBtn;

    /// <value> Menu d'actions </value>
    [SerializeField] GameObject modeAndActionsMenu;
    /// <value> Menu d'options de l'objet </value>
    [SerializeField] GameObject shapeOptionsMenu;

    /// <value> Bouton suppression de vertices </value>
    [SerializeField] Button delVertBtn;
    /// <value> Bouton ajout d'un vertex </value>
    [SerializeField] Button addVertBtn;
    /// <value> Bouton ajout d'un triangle </value>
    [SerializeField] Button addTriBtn;
    /// <value> Bouton suppression d'un triangle </value>
    [SerializeField] Button delTriBtn;
    /// <value> Bouton déselection des vertices </value>
    [SerializeField] Button deselectBtn;

    /// <value> Champ de texte affichant le nombre de vertices selectionnés </value>
    [SerializeField] TMP_Text selectionNb;

    /// <value> Couleur du menu affiché </value>
    Color currentMenuBtn = new Color(0, 128, 0);
    /// <value> Couleur de 2nd menu </value>
    Color otherMenuBtn = new Color(255, 255, 255);

    /// <value> Derniere valeur du nombre de vertices dans la selection </value>
    private int lastSelectionCount = 0;


    /// <summary>
    /// Start du GameObject
    /// </summary>
    void Start()
    {
        shapeOptionsMenu.SetActive(false);
        modeAndActionsMenu.SetActive(true);
        modeAndActionsBtn.GetComponent<Image>().color = currentMenuBtn;
        shapeOptionsBtn.GetComponent<Image>().color = otherMenuBtn;
        delVertBtn.interactable = false;
        addVertBtn.interactable = false;
        addTriBtn.interactable = false;
        delTriBtn.interactable = false;
        deselectBtn.interactable = false;
        selectionNb.text = "0";
    }


    /// <summary>
    /// Update du GameObject a chaque frame pour rendre interractable seulemetn les actions disponibles
    /// </summary>
    void Update()
    {
        if (SelectionManager.GetSelectedVertices().Count != lastSelectionCount)
        {
            switch (SelectionManager.GetSelectedVertices().Count)
            {
                // Aucun vertex selectionné. Aucune action n'est disponible
                case 0:
                    delVertBtn.interactable = false;
                    addVertBtn.interactable = false;
                    addTriBtn.interactable = false;
                    delTriBtn.interactable = false;
                    deselectBtn.interactable = false;
                    break;

                // 1 vertex de selectionné. On peut le supprimer ou tout deselctionné
                case 1:
                    delVertBtn.interactable = true;
                    deselectBtn.interactable = true;

                    addVertBtn.interactable = false;
                    addTriBtn.interactable = false;
                    delTriBtn.interactable = false;
                    break;

                // 2 vertices de selectionnés. On peut les supprimer, ajouter un vertex entre ou tout deselctionné
                case 2:
                    addVertBtn.interactable = true;

                    addTriBtn.interactable = false;
                    delTriBtn.interactable = false;
                    break;

                // 3 vertices de selectionnés. On peut les supprimer, ajouter un triangle, supprimer un triangle entre ou tout deselctionné
                case 3:
                    addTriBtn.interactable = true;

                    List<GameObject> vertex = SelectionManager.GetSelectedVertices();
                    List<NetworkVertexController> neighbours = vertex[0].GetComponent<NetworkVertexController>().neighbours;
                    if (neighbours.Contains(vertex[1].GetComponent<NetworkVertexController>())
                    && neighbours.Contains(vertex[2].GetComponent<NetworkVertexController>()))
                    {
                        delTriBtn.interactable = true;
                    }

                    addVertBtn.interactable = false;
                    break;

                // 4 vertices de selectionnés. On peut les supprimer ou tout deselctionné
                case 4:
                    addTriBtn.interactable = false;
                    delTriBtn.interactable = false;
                    break;
            }


            // Affichage du nombre de vertices selectionné
            selectionNb.text = SelectionManager.GetSelectedVertices().Count.ToString();
            lastSelectionCount = SelectionManager.GetSelectedVertices().Count;
        }
    }


    /// <summary>
    /// Change de menu sur la main
    /// </summary>
    /// <param name="menu">
    /// Le menu a affiché
    /// </param>
    public void ChangeMenu(string menu)
    {
        switch (menu)
        {
            case "ModeAndActions":
                shapeOptionsMenu.SetActive(false);
                modeAndActionsMenu.SetActive(true);
                modeAndActionsBtn.GetComponent<Image>().color = currentMenuBtn;
                shapeOptionsBtn.GetComponent<Image>().color = otherMenuBtn;
                break;

            case "ShapeOptions":
                modeAndActionsMenu.SetActive(false);
                shapeOptionsMenu.SetActive(true);
                shapeOptionsBtn.GetComponent<Image>().color = currentMenuBtn;
                modeAndActionsBtn.GetComponent<Image>().color = otherMenuBtn;
                break;

            default:
                break;
        }
    }


    /// <summary>
    /// Bouton suppression de vertices 
    /// </summary>
    /// <seealso cref="NetworkObject.DeleteVertex(int[])"/>
    public void DeleteVertex()
    {
        // Recuperations des spheres selectionnés
        List<GameObject> verticesObjects = SelectionManager.GetSelectedVertices();

        // Récuperation des indices des vertices dans la tableau de la mesh
        List<int> selectedVertices = new List<int>();
        foreach (GameObject item in verticesObjects)
        {
            selectedVertices.Add(item.GetComponent<NetworkVertexController>().index);
        }

        // Clear the selection
        SelectionManager.ClearSelection();

        // RPC
        GameObject shape = GameObject.FindWithTag("NetworkObject");
        shape.GetComponent<PhotonView>().RPC("DeleteVertex", RpcTarget.All, selectedVertices.ToArray());

    }


    /// <summary>
    /// Bouton ajout d'un vertex
    /// </summary>
    /// <seealso cref="NetworkObject.AddVertex(int, int)"/>
    public void AddVertex()
    {
        // Seulement si 2 vertices sont selectionnés
        if (SelectionManager.GetSelectedVertices().Count == 2)
        {
            // Recuperations des spheres selectionnés
            List<GameObject> verticesObjects = SelectionManager.GetSelectedVertices();

            // Récuperation des indices des vertices dans la tableau de la mesh
            List<int> selectedVertices = new List<int>();
            foreach (GameObject item in verticesObjects)
            {
                selectedVertices.Add(item.GetComponent<NetworkVertexController>().index);
            }

            // RPC
            GameObject shape = GameObject.FindWithTag("NetworkObject");
            shape.GetComponent<PhotonView>().RPC("AddVertex", RpcTarget.All, selectedVertices[0], selectedVertices[1]);

            // Clear the selection
            SelectionManager.ClearSelection();

        }
    }


    /// <summary>
    /// Bouton creation d'un triangle
    /// </summary>
    /// <seealso cref="NetworkObject.CreateTriangle(int, int, int)"/>
    public void CreateTriangle()
    {
        // Seulement si 3 vertices sont selectionnés
        if (SelectionManager.GetSelectedVertices().Count == 3)
        {
            // Recuperations des spheres selectionnés
            List<GameObject> verticesObjects = SelectionManager.GetSelectedVertices();

            // Récuperation des indices des vertices dans la tableau de la mesh
            List<int> selectedVertices = new List<int>();
            foreach (GameObject item in verticesObjects)
            {
                selectedVertices.Add(item.GetComponent<NetworkVertexController>().index);
            }

            // RPC
            GameObject shape = GameObject.FindWithTag("NetworkObject");
            shape.GetComponent<PhotonView>().RPC("CreateTriangle", RpcTarget.All, selectedVertices[0], selectedVertices[1], selectedVertices[2]);


            // Clear the selection
            SelectionManager.ClearSelection();
        }
    }




    /// <summary>
    /// Bouton suppression d'un triangle
    /// </summary>
    /// <seealso cref="NetworkObject.DeleteTriangle(int, int, int)"/>
    public void DeleteTriangle()
    {
        // Seulement si 3 vertices sont selectionnés
        if (SelectionManager.GetSelectedVertices().Count == 3)
        {
            // Recuperations des spheres selectionnés
            List<GameObject> verticesObjects = SelectionManager.GetSelectedVertices();

            // Récuperation des indices des vertices dans la tableau de la mesh
            List<int> selectedVertices = new List<int>();
            foreach (GameObject item in verticesObjects)
            {
                selectedVertices.Add(item.GetComponent<NetworkVertexController>().index);
            }

            // RPC
            GameObject shape = GameObject.FindWithTag("NetworkObject");
            shape.GetComponent<PhotonView>().RPC("DeleteTriangle", RpcTarget.All, selectedVertices[0], selectedVertices[1], selectedVertices[2]);

            // Clear the selection
            SelectionManager.ClearSelection();
        }
    }


    /// <summary>
    /// Bouton de déselection
    /// </summary>
    /// <seealso cref="SelectionManager.ClearSelection"/>
    public void DeselectAll()
    {
        // Clear the selection
        SelectionManager.ClearSelection();
    }
}
