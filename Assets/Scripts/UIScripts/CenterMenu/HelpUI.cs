﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Panneau d'aide
/// </summary>
public class HelpUI : MonoBehaviour
{

    /// <summary>
    /// Retourne au panneau principal du Game Menu. La sauvegarde est abandonnée.
    /// </summary>
    public void CancelBtn()
    {
        gameObject.SetActive(false);
    }
}
