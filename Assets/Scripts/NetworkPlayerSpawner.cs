﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using UnityEngine;
using Photon.Pun;


/// <summary>
/// Gêre la création et la destruction des avatars réseau
/// </summary>
public class NetworkPlayerSpawner : MonoBehaviourPunCallbacks
{
    /// <value> Nombre de couleurs differentes pour les avatars </value>
    [SerializeField] const int nbColors = 10;

    /// <value> Tableau des couleurs disponibles </value>
    [SerializeField] Color[] colors = new Color[nbColors];

    /// <value> La couleur de notre avatar </value>
    public static Color myColor;

    /// <value> Prefab de notre avatar </value>
    private GameObject spawnedPlayerPrefab;


    /// <summary>
    /// Callback fournie par Photon lorsque l'on rejoint une room. 
    /// On créé un avatar réseau nous représentant et on l'instantie a une position qui dépend 
    /// du nombre de joueurs déjà présents, pour éviter de mettre tous les joueurs au même endroit (un dans l'autre)
    /// </summary>
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        // get avatars in room
        int PlayerNb = PhotonNetwork.LocalPlayer.ActorNumber - 1;

        // compute player's initial position
        int posPlayer = 0;
        if (PlayerNb % 2 == 0)
        {
            posPlayer = (PlayerNb / 2) % 80;  // 1m to the right
        }
        else
        {
            posPlayer = (-(PlayerNb + 1) / 2) % 80;   // 1m to the left
        }
        Vector3 playerPos = new Vector3(posPlayer, 0, -5);

        // choose player's color
        myColor = colors[PlayerNb % nbColors];
        
        // instantiate our avatar
        spawnedPlayerPrefab = PhotonNetwork.Instantiate("Network Player", playerPos, transform.rotation);
    }


    /// <summary>
    /// Callback fournie par Photon lorque l'on quitte une room.
    /// On détruit notre avatar réseau.
    /// </summary>
    public override void OnLeftRoom()
    {
        if (spawnedPlayerPrefab != null)
        {
            PhotonNetwork.Destroy(spawnedPlayerPrefab);
        }
        base.OnLeftRoom();
    }
}
