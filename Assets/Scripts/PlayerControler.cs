﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////


using UnityEngine;
using Photon.Pun;
using UnityEngine.XR.Interaction.Toolkit;


/// <summary>
/// Gêre les inputs du joueur.
/// </summary>
public class PlayerControler : MonoBehaviour
{
    /// <value> Rayon de TP </value>
    [SerializeField] GameObject teleportRay;
    /// <value> Rayon de selection main droite </value>
    [SerializeField] GameObject rightSelectRay;
    /// <value> Rayon de selection main gauche </value>
    [SerializeField] GameObject leftSelectRay;

    /// <value> Transform Component de la manette gauche </value>
    [SerializeField] Transform leftController;

    /// <value> Menu de sauvegarde et d'infos (Game Menu) </value>
    [SerializeField] GameObject menus;

    /// <value> Menu d'action de la main gauche (Hand Menu) </value>
    [SerializeField] GameObject modeAndActionsMenu;

    /// <value> Largeur de la salle </value>
    [SerializeField] float roomWidth = 90.0f;
    /// <value> Profondeur de la salle </value>
    [SerializeField] float roomDepth = 90.0f;

    /// <value> Vaut vrai si le rayon de selection de la main gauche est actif </value>
    bool wasActiveLRaylight = true;
    /// <value> Vaut vrai si le rayon de selection de la main droite est actif </value>
    bool wasActiveRRaylight = true;

    /// <value> Enum représentant les 3 axes de rotations </value>
    public enum RotationAxis { X=0, Y=1, Z=2 };
    /// <value> Axe de rotation séléctionné </value>
    public static RotationAxis selectedAxis;

    /// <value> Objet réseau </value>
    private static GameObject shape = null;
    /// <value> Avatar du joueur </value>
    private NetworkPlayer avatar = null;


    /// <summary>
    /// Affecte l'objet sur lequel on travail.
    /// </summary>
    /// <param name="networkObject">
    /// le Gameobject représentant l'objet
    /// </param>
    public static void setNetworkObject(GameObject networkObject)
    {
        shape = networkObject;
    }

    /// <summary>
    /// Affecte l'avatar réseau du joueur.
    /// </summary>
    /// <param name="networkPlayer">
    /// Avatar réseau
    /// </param>
    public void setNetworkPlayer(NetworkPlayer networkPlayer)
    {
        avatar = networkPlayer;
    }


    /// <summary>
    /// Start du GameObject. Attribution des valeurs par défaut.
    /// </summary>
    private void Start()
    {
        selectedAxis = RotationAxis.Y;
        modeAndActionsMenu.SetActive(true);
    }


    /// <summary>
    /// Update du GameObject à chaque frame. Actions en fonctions des inputs.
    /// </summary>
    void Update()
    {
        OVRInput.Update();
        
        // Si l'objet peut être déplacé
        if (SelectionManager.isEmpty() && NetworkObject.canMove==0)
        {
            if (shape != null)
            {
                // right thumbstick changes object's position
                if (OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick).magnitude > 0.1f)
                {
                    // if not owner request ownership
                    PhotonView photonView = shape.GetComponent<PhotonTransformView>().photonView;
                    if (!photonView.IsMine)
                    {
                        photonView.RequestOwnership();
                    }

                    // shape doesn't get out of the limits
                    Vector3[] corners = getCornersPos(shape.GetComponent<NetworkObject>());
                    Vector2 axis = validDisplacement(OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick), corners);

                    // translate object
                    Transform obj = shape.transform;
                    Vector3 direction = leftController.forward;
                    float x = axis.x;
                    float y = axis.y;
                    float speed = 0.1f;  

                    // new position relative to the left controller's orientation
                    obj.position = new Vector3(
                        obj.position.x + (speed * (x * direction.z + y * direction.x)),
                        obj.position.y,
                        obj.position.z + (speed * (y * direction.z - x * direction.x))
                    );
                    shape.GetComponent<NetworkObject>().UpdateMinMax();
                }

                // left thumbstick changes object's rotation
                if (OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).magnitude > 0.1f)
                {
                    // if not owner request ownership
                    PhotonView photonView = shape.GetComponent<PhotonTransformView>().photonView;
                    if (!photonView.IsMine)
                    {
                        photonView.RequestOwnership();
                    }

                    // shape doesn't get out of the limits
                    Vector3[] corners = getCornersPos(shape.GetComponent<NetworkObject>());
                    bool positive = isPositiveRotation(shape.transform.eulerAngles.y);
                    Vector2 rotation = validRotation(OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick), corners, positive);

                    // rotate object
                    Transform obj = shape.transform;
                    float speed = 0.5f;
                    switch (selectedAxis)
                    {
                        case RotationAxis.X:
                            obj.Rotate(new Vector3(speed * rotation.x, 0, 0));
                            break;
                        case RotationAxis.Y:
                            obj.Rotate(new Vector3(0, speed * rotation.x, 0));
                            break;
                        case RotationAxis.Z:
                            obj.Rotate(new Vector3(0, 0, speed * rotation.x));
                            break;
                        default:
                            break;
                    }
                    shape.GetComponent<NetworkObject>().UpdateMinMax();
                }
            }
            
        }

        // A button switches select/teleport mode
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            swichTeleportAndSelectMode();
        }

        // X button opens/closes hand menu
        if (OVRInput.GetDown(OVRInput.Button.Three))
        {
            modeAndActionsMenu.SetActive(!modeAndActionsMenu.activeInHierarchy);
            SyncAvatarHandMenu();
            //  menu opening
            if (modeAndActionsMenu.activeInHierarchy)
            {
                // get back to select mode
                if (teleportRay.activeInHierarchy)
                {
                    swichTeleportAndSelectMode();
                }

                // activate raylights
                rightSelectRay.SetActive(true);
                SyncAvatarRaylight(false);
            }
            else
            {
                // set back raylights
                leftSelectRay.SetActive(wasActiveLRaylight);
                rightSelectRay.SetActive(wasActiveRRaylight);
                SyncAvatarRaylight(true);
                SyncAvatarRaylight(false);
            }
        }

        // left menu button opens/closes game menu
        if (OVRInput.GetDown(OVRInput.Button.Start))
        {
            menus.SetActive(!menus.activeInHierarchy);
            SyncAvatarGameMenu();

            //  menu opening
            if (menus.activeInHierarchy)
            {
                // get back to select mode
                if (teleportRay.activeInHierarchy)
                {
                    swichTeleportAndSelectMode();
                }

                // activate raylights
                leftSelectRay.SetActive(true);
                rightSelectRay.SetActive(true);
                SyncAvatarRaylight(true);
                SyncAvatarRaylight(false);
            }
            else
            {
                // set back raylights
                leftSelectRay.SetActive(wasActiveLRaylight);
                rightSelectRay.SetActive(wasActiveRRaylight);
                SyncAvatarRaylight(true);
                SyncAvatarRaylight(false);
            }
            
        }

        // Selection / deselection du vertex avec la touche trigger gauche
        if (OVRInput.GetDown(OVRInput.RawButton.LIndexTrigger))
        {
            // if raylight hits a vertex
            if (leftSelectRay.GetComponent<XRRayInteractor>().GetCurrentRaycastHit(out RaycastHit hit)
            && hit.transform.TryGetComponent(out NetworkVertexController vertex))
            {
                // if vertex selected, deselect it
                if (SelectionManager.GetSelectedVertices().Contains(vertex.gameObject))
                {
                    SelectionManager.deselectVertex(vertex.gameObject);
                    vertex.isInSelectedList = false;
                    vertex.GetComponent<Renderer>().material.SetColor("BaseColor", vertex.unblockedVertex);
                    vertex.mainObject.UnblockVertex(vertex.index);
                }
                // if vertex not selected, select it
                else
                {
                    SelectionManager.selectVertex(vertex.gameObject);
                    vertex.isInSelectedList = true;
                    vertex.GetComponent<Renderer>().material.SetColor("BaseColor", vertex.selectedVertex);
                    vertex.mainObject.BlockVertex(vertex.index);
                }
            }
        }

        // Selection / deselection du vertex avec la touche trigger droit
        if (OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger))
        {
            // if raylight hits a vertex
            if (rightSelectRay.GetComponent<XRRayInteractor>().GetCurrentRaycastHit(out RaycastHit hit) 
            && hit.transform.TryGetComponent(out NetworkVertexController vertex))
            {
                // if vertex selected, deselect it
                if (SelectionManager.GetSelectedVertices().Contains(vertex.gameObject))
                {
                    SelectionManager.deselectVertex(vertex.gameObject);
                    vertex.isInSelectedList = false;
                    vertex.GetComponent<Renderer>().material.SetColor("BaseColor", vertex.unblockedVertex);
                    vertex.mainObject.UnblockVertex(vertex.index);
                }
                // if vertex not selected, select it
                else
                {
                    SelectionManager.selectVertex(vertex.gameObject);
                    vertex.isInSelectedList = true;
                    vertex.GetComponent<Renderer>().material.SetColor("BaseColor", vertex.selectedVertex);
                    vertex.mainObject.BlockVertex(vertex.index);
                }
            }
        }

        if (!menus.activeInHierarchy)
        {
            // Y enables/disables raylight if menu not open
            if (OVRInput.GetDown(OVRInput.Button.Four))
            {
                leftSelectRay.SetActive(!leftSelectRay.activeInHierarchy);
                SyncAvatarRaylight(true);
                wasActiveLRaylight = !wasActiveLRaylight;
            }

            // select mode only
            if (!teleportRay.activeInHierarchy)
            {
                // B enables/disables raylight if menu not open
                if (!modeAndActionsMenu.activeInHierarchy && OVRInput.GetDown(OVRInput.Button.Two))
                {
                    rightSelectRay.SetActive(!rightSelectRay.activeInHierarchy);
                    SyncAvatarRaylight(false);
                    wasActiveRRaylight = !wasActiveRRaylight;
                }
            }
        }
    }


    /// <summary>
    /// On appelle la fonction de notre avatar réseau pour synchroniser l'activation de nos rayons.
    /// </summary>
    /// <param name="left">
    /// Vaut vrai s'il s'agit du rayon gauche, faux s'il s'agit du droit.
    /// </param>
    void SyncAvatarRaylight(bool left)
    {
        bool activate = left ? leftSelectRay.activeInHierarchy : rightSelectRay.activeInHierarchy;
        avatar.SyncRaylight(left, activate);
    }

    /// <summary>
    /// On appelle la fonction de notre avatar réseau pour synchroniser l'activation du Hand Menu.
    /// </summary>
    void SyncAvatarHandMenu()
    {
        avatar.SyncHandMenu(modeAndActionsMenu.activeInHierarchy);
    }

    /// <summary>
    /// On appelle la fonction de notre avatar réseau pour synchroniser l'activation du Game Menu.
    /// </summary>
    void SyncAvatarGameMenu()
    {
        avatar.SyncGameMenu(menus.activeInHierarchy);
    }


    /// <summary>
    /// Permet de switch le rayon de la mains droite entre le mode de grab/sélection et le mode téléportation
    /// </summary>
    public void swichTeleportAndSelectMode()
    {
        teleportRay.SetActive(!teleportRay.activeInHierarchy);

        // set back select ray after teleportation
        if (!teleportRay.activeInHierarchy && wasActiveRRaylight)
        {
            rightSelectRay.SetActive(true);
        }
        else if (teleportRay.activeInHierarchy)
        {
            rightSelectRay.SetActive(false);
        }
    }


    /// <summary>
    /// Récupération des positions des extremités de l'objet (boîte englobante) en world position.
    /// </summary>
    /// <param name="shape">
    /// L'objet réseau
    /// </param>
    /// <returns>
    /// Les 8 angles du parallélépipède rectangle qui correspond à la boîte englobante de l'objet.
    /// </returns>
    private Vector3[] getCornersPos(NetworkObject shape)
    {
        // corners' world positions
        Vector3[] corners = {
            new Vector3(shape.minX, shape.minY, shape.minZ),
            new Vector3(shape.minX, shape.minY, shape.maxZ),
            new Vector3(shape.maxX, shape.minY, shape.maxZ),
            new Vector3(shape.maxX, shape.minY, shape.minZ),
            new Vector3(shape.minX, shape.maxY, shape.minZ),
            new Vector3(shape.minX, shape.maxY, shape.maxZ),
            new Vector3(shape.maxX, shape.maxY, shape.maxZ),
            new Vector3(shape.maxX, shape.maxY, shape.minZ)
        };

        return corners;
    }


    /// <summary>
    /// On restreint les déplacements pour que l'objet soit toujours dans la salle.
    /// Attention, la hauteur de la pièce n'est pas prise en compte.
    /// </summary>
    /// <param name="direction">
    /// Direction du déplacement.
    /// </param>
    /// <param name="corners">
    /// Les extrémités de l'objet.
    /// </param>
    /// <returns>
    /// Un vecteur de translation qui ne fait pas sortir l'objet de la salle.
    /// </returns>
    private Vector2 validDisplacement(Vector2 direction, Vector3[] corners)
    {
        foreach (Vector3 corner in corners)
        {
            // si un coin sort de la salle sur l'axe x, la translation en x vaut 0
            if ((direction.x > 0 && corner.x > roomWidth / 2.0f) || (direction.x < 0 && corner.x < -roomWidth / 2.0f)) {
                direction.x = 0.0f;
            }

            // si un coin sort de la salle sur l'axe z, la translation en y (car translation en 2D) vaut 0
            if ((direction.y > 0 && corner.z > roomDepth / 2.0f) || (direction.y < 0 && corner.z < -roomDepth / 2.0f)) {
                direction.y = 0.0f;
            }
        }

        return direction;
    }


    /// <summary>
    /// On restreient les rotations pour que l'objet soit toujours dans la salle.
    /// Attention, la hauteur de la pièce n'est pas prise en compte.
    /// </summary>
    /// <param name="rotation">
    /// Vecteur dont la première coordonnée correspond à la rotation.
    /// </param>
    /// <param name="corners">
    /// Extremités de l'objet.
    /// </param>
    /// <param name="positive">
    /// Orientation actuelle de l'objet, vaut vrai pour un angle saillant positif et faux pour un angle saillant négatif.
    /// </param>
    /// <returns>
    /// Un vecteur dont la permière coordonnée correspond à une rotation qui ne fait pas sortir l'objet de la salle.
    /// </returns>
    private Vector2 validRotation(Vector2 rotation, Vector3[] corners, bool positive)
    {
        foreach (Vector3 corner in corners)
        {
            // si la rotation et l'orientation actuelle de l'objet sont dans le même sens
            if ((rotation.x > 0 && positive) || (rotation.x < 0 && !positive))
            {
                // si un angle dépasse les limites de la pièce la rotation est nulle
                if ((corner.x > roomWidth / 2.0f) || (corner.x < -roomWidth / 2.0f)
                 || (corner.z > roomDepth / 2.0f) || (corner.z < -roomDepth / 2.0f))
                {
                    rotation.x = 0.0f;
                }
            }
        }

        return rotation;
    }


    /// <summary>
    /// Verifie si la rotation qui correspond à l'angle minimal (angle saillant) se fait dans le sens horaire. 
    /// </summary>
    /// <param name="angle">
    /// L'angle orienté en degrés.
    /// </param>
    /// <returns>
    /// Vrai si la rotation est dans le sens horaire, faux si elle est dans le sens anti-horaire.
    /// </returns>
    private bool isPositiveRotation(float angle)
    {
        // transforme l'angle en angle saillant orienté
        angle = angle % 360.0f;
        if (angle > 180.0f)
        {
            angle = -(360.0f - angle);
        }
        else if (angle < -180.0f)
        {
            angle = 360.0f + angle;
        }

        return angle >= 0;
    }

}
