﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;

/// <summary>
/// Initialise l'affichage des panneaux du canvas de gauche du Game Menu.
/// </summary>
public class LeftMenuControler : MonoBehaviour
{
    /// <summary>
    /// Désacitve tous les panneaux du canvas, sauf le premier (l'affichage du challenge), à chaque ouverture du menu.
    /// </summary>
    void OnEnable()
    {
        // display challenge menu
        foreach (Transform panel in transform)
        {
            panel.gameObject.SetActive(false);
        }
        transform.GetChild(0).gameObject.SetActive(true);
    }
}
