﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////

using TMPro;
using UnityEngine;

/// <summary>
/// Affichage du message corrsepondant au résultat du challenge.
/// </summary>
public class ResultChallengeUI : MonoBehaviour
{
    /// <value>Panneau d'affichage du challenge</value>
    [SerializeField] GameObject challengePanel;

    /// <value>Message de succès ou d'échec du challenge</value>
    public TMP_Text message;

    /// <summary>
    /// Retour au panneau d'affichage du challenge.
    /// </summary>
    public void OKButton()
    {
        gameObject.SetActive(false);
        challengePanel.SetActive(true);
    }
}
