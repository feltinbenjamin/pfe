﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//                                  Projet de Fin d'Etudes
//                      Collaboration intéractive en réalité virtuelle
//
//                 Auteurs :    Ambre Payan - ambre.payan@etu.univ-amu.fr
//                              Benjamin Feltin - benjamin.feltin@etu.univ-amu.fr
//
//  Enseignants encadrants :    Marc Daniel - marc.daniel@univ-amu.fr
//                              Sebastien Mavromatis - sebastien.mavromatis@univ-amu.fr
//
//////////////////////////////////////////////////////////////////////////////////////////

using Photon.Pun;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Panneau de gestion des options concernant la forme.
/// </summary>
public class ShapeOptionUI : MonoBehaviour
{
    /// <value>Slider contrôlant le grossissement de l'objet réseau</value>
    [SerializeField] Slider scaleSlider;
    /// <value>Slider contrôlant la position sur l'axe vertical de l'objet réseau</value>
    [SerializeField] Slider heightSlider;

    /// <value>Case à cocher inidquant l'utilisation du mode de coloration en fonction de l'élévation</value>
    [SerializeField] Toggle toggleElevation;

    /// <value>Champ de texte contenant le grossissement minimal de l'objet.</value>
    [SerializeField] TMP_Text minScale;
    /// <value>Champ de texte contenant le grossissement maximal de l'objet.</value>
    [SerializeField] TMP_Text maxScale;
    /// <value>Champ de texte contenant le grossissement actuel de l'objet.</value>
    [SerializeField] TMP_Text currentScale;

    /// <value>Champ de texte contenant la position minimale sur l'axe vertical de l'objet.</value>
    [SerializeField] TMP_Text minHeight;
    /// <value>Champ de texte contenant la position maximale sur l'axe vertical de l'objet.</value>
    [SerializeField] TMP_Text maxHeight;
    /// <value>Champ de texte contenant la position actuelle sur l'axe vertical de l'objet.</value>
    [SerializeField] TMP_Text currentHeight;

    /// <value>Grossissement minimal de l'objet. La valeur doit être strictement positive. 
    /// Une veleur inférieure à 1 correspond à un rétrécissement.</value>
    [SerializeField] float minScaleVal;
    /// <value>Grossissement maximal de l'objet. La valeur doit être supérieure à minScaleVal. 
    /// Une veleur inférieure à 1 correspond à un rétrécissement.</value>
    [SerializeField] float maxScaleVal;
    /// <value>Grossissement par défaut de l'objet. La valeur doit être comprise entre minScaleVal et maxScaleVal.</value>
    [SerializeField] float defaultScale;

    /// <value>Position minimale sur l'axe vertical de l'objet. 
    /// Attention, une valeur négative placera l'origine de l'objet sous le sol.</value>
    [SerializeField] float minHeightVal;
    /// <value>Position maximale sur l'axe vertical de l'objet. La valeur doit être supérieure à minHeightVal.
    /// Attention, une valeur négative placera l'origine de l'objet sous le sol.</value>
    [SerializeField] float maxHeightVal;
    /// <value>Champ de texte contenant la position par défaut sur l'axe vertical de l'objet. 
    /// La valeur doit être comprise entre minHeightVal et maxHeightVal.</value>
    [SerializeField] float defaultHeight;

    /// <value>Préfab d'un vertex.</value>
    [SerializeField] GameObject vertexPrefab;

    /// <value>Position de l'objet sur l'axe vertical lors de la dernière mise à jour.</value>
    private float lastHeight;
    /// <value>Grossissement de l'objet lors de la dernière mise à jour.</value>
    private float lastScale;

    /// <value>Valeur de la variable canMove lors de la dernière mise à jour.</value>
    private int lastCanMove = 0;

    /// <value>L'objet réseau.</value>
    private static GameObject shape = null;
    /// <value>Taille d'un vertex indépendemment du grossissement de l'objet.</value>
    private float vertexScale = -1.0f;

    /// <summary>
    /// Affectation de l'objet réseau.
    /// </summary>
    /// <param name="networkObject">Le gameobject associé à l'objet réseau.</param>
    public static void setNetworkObject(GameObject networkObject)
    {
        shape = networkObject;
    }

    /// <summary>
    /// Activation/désactivation des sliders d'échelle et de hauteur en fonction des blocages.
    /// </summary>
    /// <param name="canMove">Si vrai les sliders sont débloqués, si faux les sliders sont bloqués.</param>
    void setDisplacementsAbility(bool canMove)
    {
        scaleSlider.interactable = canMove;
        heightSlider.interactable = canMove;
    }

    /// <summary>
    /// Initialisations des valeurs des sliders, de la taille des vertex et des possibilités de déplacement.
    /// </summary>
    void Awake()
    {
        vertexScale = vertexPrefab.transform.localScale.x;
        minScale.text = minScaleVal.ToString();
        maxScale.text = maxScaleVal.ToString();
        minHeight.text = minHeightVal.ToString();
        maxHeight.text = maxHeightVal.ToString();
        SetScaleSlider(scaleSlider);
        SetHeightSlider(heightSlider);
        setDisplacementsAbility(true);
    }

    /// <summary>
    /// Mise à jour des valeurs des sliders et des blocages.
    /// </summary>
    private void Update()
    {
        if (shape != null)
        {
            // update scale slider's position
            if(Math.Abs(shape.transform.localScale.x - lastScale) > 1e-2)
            {
                SetScaleSlider(scaleSlider);
            }

            // update height slider's position
            if (Math.Abs(shape.transform.position.y - lastHeight) > 1e-2)
            {
                SetHeightSlider(heightSlider);
            }

            // set sliders interactability
            if (NetworkObject.canMove != lastCanMove)
            {
                // set interactable
                if (NetworkObject.canMove == 0)
                {
                    setDisplacementsAbility(true);
                }
                // set not interactable
                else if (lastCanMove == 0)
                {
                    setDisplacementsAbility(false);
                }
                // update lastCanMove
                lastCanMove = NetworkObject.canMove;
            }
        }
    }

    /// <summary>
    /// Change l'axe de rotation de l'objet réseau en fonction de la valeur passée en paremètre.
    /// </summary>
    /// <param name="axis">
    /// <list type="bullet">
    /// <item>
    /// <term>X</term>
    /// <description>Premier axe horizontal</description>
    /// </item>
    /// <item>
    /// <term>Y</term>
    /// <description>Axe vertical</description>
    /// </item>
    /// <item>
    /// <term>Z</term>
    /// <description>Deuxième axe horizontal</description>
    /// </item>
    /// </list></param>
    public void ChooseRotationAxis(string axis)
    {
        switch (axis)
        {
            case "X":
                PlayerControler.selectedAxis = PlayerControler.RotationAxis.X;
                break;
            case "Y":
                PlayerControler.selectedAxis = PlayerControler.RotationAxis.Y;
                break;
            case "Z":
                PlayerControler.selectedAxis = PlayerControler.RotationAxis.Z;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Change le material utiliser pour afficher l'objet réseau en fonction de la valeur passée en argument.
    /// </summary>
    /// <param name="mode">
    /// <list type="bullet">
    /// <item>
    /// <term>Default</term>
    /// <description>Rendu par défaut</description>
    /// </item>
    /// <item>
    /// <term>Wireframe</term>
    /// <description>Rendu wireframe de l'objet</description>
    /// </item>
    /// <item>
    /// <term>Combined</term>
    /// <description>Mode combiné, les aretes des triangles sont dessiné par dessus la mesh</description>
    /// </item>
    /// </list></param>
    /// <seealso cref="NetworkObject.ChooseMaterial(string)"/>
    public void ChooseMaterial(string mode)
    {
        if (shape != null)
        {
            NetworkObject objectScript = shape.GetComponent<NetworkObject>();
            objectScript.ChooseMaterial(mode);
            ChooseShader(toggleElevation.isOn);
        }
    }

    /// <summary>
    /// Si la case est cochée, le shader utilisera le mode de coloration en fonction de l'élévetion.
    /// </summary>
    /// <param name="val">Booléen indiquant si la case est cochée.</param>
    public void ChooseShader(bool val)
    {
        if (shape != null)
        {
            // transformation du booléen en float pour le shader
            float floatVal = val ? 1.0f : 0.0f;
            // mise à jour de la valeur du shader
            shape.GetComponent<MeshRenderer>().materials[0].SetFloat("ElevationColorMode", floatVal);
            // mise à jour des valeurs min max du shader
            shape.GetComponent<MeshRenderer>().materials[0].SetVector("MinMaxPositions", 
                new Vector2(shape.GetComponent<NetworkObject>().minY, shape.GetComponent<NetworkObject>().maxY));
        }
    }

    /// <summary>
    /// Change la taille de l'objet réseau en fonction de la valeur du slider, en utilisant une échelle logarithmique, 
    /// tout en conservant la taille des vertex.
    /// </summary>
    /// <param name="value">Valeur du slider</param>
    public void ScaleSlider(float value)
    {
        if (shape != null && NetworkObject.canMove==0)
        {
            // if not owner request ownership
            PhotonView photonView = shape.GetComponent<PhotonTransformView>().photonView;
            if (!photonView.IsMine)
            {
                photonView.RequestOwnership();
            }

            // compute new scale value with logarithmic scale
            float newVal = Mathf.Pow(10, Mathf.Log10(minScaleVal) + (value * (Mathf.Log10(maxScaleVal) - Mathf.Log10(minScaleVal))));

            // update current value text
            currentScale.text = (Mathf.Round(newVal * 100.0f) / 100.0f).ToString();

            // change shape's scale
            shape.transform.localScale = new Vector3(newVal, newVal, newVal);
            shape.GetComponent<NetworkObject>().UpdateMinMax();

            // keep absolute vertices scale
            foreach (Transform vertex in shape.transform)
            {
                vertex.localScale = new Vector3(vertexScale / newVal, vertexScale / newVal, vertexScale / newVal);
            }
        }
    }

    /// <summary>
    /// Met à jour le slider en fonction de la taille de l'objet réseau, en utilisant une échelle logarithmique.
    /// </summary>
    /// <param name="slider">Slider à mettre à jour</param>
    public void SetScaleSlider(Slider slider)
    {
        if (shape != null)
        {
            float scale = shape.transform.localScale.x;
            // set slider value with logarithmic scale
            slider.value = (Mathf.Log10(scale) - Mathf.Log10(minScaleVal)) / (Mathf.Log10(maxScaleVal) - Mathf.Log10(minScaleVal));
            lastScale = scale;
        }
    }

    /// <summary>
    /// Restaure la taille de l'objet par défaut si le changement d'échelle n'est pas bloqué.
    /// </summary>
    /// <param name="slider">Slider à mettre à jour</param>
    public void DefaultScaleBtn(Slider slider)
    {
        // change slider position with logarithmic scale
        if (NetworkObject.canMove == 0)
            slider.value = (Mathf.Log10(defaultScale) - Mathf.Log10(minScaleVal)) / (Mathf.Log10(maxScaleVal) - Mathf.Log10(minScaleVal));
    }

    /// <summary>
    /// Change la position sur l'axe vertical de l'objet réseau en fonction de la valeur du slider.
    /// </summary>
    /// <param name="value">Valeur du slider</param>
    public void HeightSlider(float value)
    {
        if (shape != null && NetworkObject.canMove==0)
        {
            // if not owner request ownership
            PhotonView photonView = shape.GetComponent<PhotonTransformView>().photonView;
            if (!photonView.IsMine)
            {
                photonView.RequestOwnership();
            }

            // compute new height value
            float newVal = minHeightVal + (value * (maxHeightVal - minHeightVal));

            // update current value
            currentHeight.text = (Mathf.Round(newVal * 100.0f) / 100.0f).ToString();

            // change shape's height
            Vector3 pos = shape.transform.position;
            shape.transform.position = new Vector3(pos.x, newVal, pos.z);
            shape.GetComponent<NetworkObject>().UpdateMinMax();
        }
    }

    /// <summary>
    /// Restaure la position sur l'axe vertical par défaut de l'objet réseau, si les déplacements ne sont pas bloqués.
    /// </summary>
    /// <param name="slider">Slider à mettre à jour</param>
    public void DefaultHeightBtn(Slider slider)
    {
        // change slider position
        if (NetworkObject.canMove == 0)
            slider.value = (defaultHeight - minHeightVal) / (maxHeightVal - minHeightVal); 
    }

    /// <summary>
    /// Met à jour la valeur du slider en fonction de la position de l'objet réseau sur l'axe vertical.
    /// </summary>
    /// <param name="slider">Slider à mettre à jour</param>
    public void SetHeightSlider(Slider slider)
    {
        if (shape != null)
        {
            float height = shape.transform.position.y;
            slider.value = (height - minHeightVal) / (maxHeightVal - minHeightVal);
            lastHeight = height;
        }
    }

}
